import React from 'react';
import Constants from './../../context/constants.jsx';
import Button from '../../control/button.jsx';
import App from '../../context/app.jsx';

class Explore extends React.Component { 

	constructor() {
        super();
        this.FILTER_LEVEL_1 = "FILTER_LEVEL_1";
        this.FILTER_LEVEL_2 = "FILTER_LEVEL_2";
        this.FILTER_TAGGED = "FILTER_TAGGED";
        this.FILTER_OTHERS = "FILTER_OTHERS";

        this.state = {
            mode: 1,
            search: "",
            activeSearch: "",
            showBranches: false,
            filter: {
                project: null,
                type: null,
                card: null,
            },
            searchList: {
                projects: []
            },
        };
    }

    componentDidMount() {
        this.onSearchClicked();
    }

    onSearchKeyUp(e) {
        if(e.key == "Enter") {
            this.onSearchClicked();
        }
    }

    onSearchChanged(e) {
        this.setState({search: e.target.value});
    }

    onSearchClicked() {
        var projects = [];
        // find all matches in our internal data structures
        var query = this.state.search.toLowerCase();
        this.state.activeSearch = query;
        App.myProjects.forEach((element) => {
            var project = null;
            if(element.name.toLowerCase().indexOf(query) != -1) {
                project = {
                    name: element.name,
                    isExpanded: true,
                    isExpandedDetails: true,
                    cards: [],
                };
                projects.push(project);
            }
            element.cards.forEach((card) => {
                var name = card.name ? card.name.toLowerCase() : "";
                var description = card.description ? card.description.toLowerCase() : "";
                if(name.indexOf(query) != -1 || description.indexOf(query) != -1) {
                    if(project == null) {
                        project = {
                            name: element.name,
                            isExpanded: true,
                            isExpandedDetails: true,
                            cards: [],
                        };
                        projects.push(project);
                    }
                    project.cards.push(card);
                }
            });
            if(project) {
                var topNodes = [];
                element.cards.forEach((card) => {
                    if(card.numIncomingConnections() == 0) {
                        topNodes.push(card);
                    }
                });
                project.topNodes = topNodes;
            }
        });

        projects.forEach((project) => {
            var taggedCards = project.cards.filter((card) => {
                return card.tags.length > 0;
            });
            var level1Cards = project.cards.filter((card) => {
                return card.numIncomingConnections() == 0;
            });
            var level2Cards = project.cards.filter((card) => {
                return card.getPath().length == 2;
            });
            var otherCards = project.cards.filter((card) => {
                return taggedCards.indexOf(card) == -1 && 
                        level1Cards.indexOf(card) == -1 && 
                        level2Cards.indexOf(card) == -1;
            });
            project.taggedCards = {
                isExpanded: false,
                cards: taggedCards,
            };
            project.level1Cards = {
                isExpanded: false,
                cards: level1Cards,
            };
            project.level2Cards = {
                isExpanded: false,
                cards: level2Cards,
            };
            project.otherCards = {
                isExpanded: false,
                cards: otherCards,
            };
        });

        // TODO: search in external databases
        
        this.setState({
            filter: {
                project: null,
                type: null,
                card: null,
            },
            searchList: {
                projects: projects
            }
        });
    }

    onSelectFilter(project, type, card) {        
        if(this.state.filter.project == project && this.state.filter.type == type && this.state.filter.card == card) {
            project = null;
            type = null;
            card = null;
        }
        this.setState({filter: {
            project: project,
            type: type,
            card: card,
        }});        
    }

    onExpandClicked(e) {
        var element = e.currentTarget;
        if(element.parentNode.parentNode.getAttribute("data-collapsed") == "true") {
            element.parentNode.parentNode.setAttribute("data-collapsed", "false");
            element.setAttribute("class", "up");
        } else {
            element.parentNode.parentNode.setAttribute("data-collapsed", "true");
            element.setAttribute("class", "down");
        }
    }

    cardName(card) {
        return card.name.length == 0 ? card.description : card.name;
    }

    createTreeView(card) {
        var tree = {
            children: []
        };
        var path = card.getPath();
        var current = null;
        for(var i=0; i<path.length; ++i) {
            var node = {
                node: path[i],
                children: []
            };
            if(current) {
                current.children.push(node);
            } else {
                tree.children.push(node);
            }
            current = node;
        }
        current.children.push(...card.getBranchPaths());
        return tree;
    }

    renderCard(card) {
        return (
            <div>
                {this.renderHighlightedText(this.cardName(card))}
                <div className="explore_card_meta">
                    <div>#{card.id}</div>
                    {card.tags.map((tag, index) => {
                        return <div key={index}>#{tag}</div>;
                    })}
                </div>
            </div>
        );
    }

    renderHighlightedText(text) {
        var i = this.state.activeSearch.length > 0 ? text.toLowerCase().indexOf(this.state.activeSearch) : -1;
        if(i >= 0) {
            var part1 = text.substring(0, i);
            var part2 = text.substring(i, i + this.state.activeSearch.length);
            var part3 = text.substring(i + this.state.activeSearch.length);
            return <span>{part1}<span data-highlighted={true}>{part2}</span>{part3}</span>;
        }
        return <span>{text}</span>;
    }

    renderSearchList() {
        var renderList = (project, group, text, id) => {
            if(group.cards.length > 0) {
                
                return (
                    <div className="explore_search_list_group">
                        <div>
                            <i 
                                className={group.isExpanded ? "up" : "down"}
                                onPointerUp={() => {group.isExpanded = !group.isExpanded; this.forceUpdate();}}/>
                            <div
                                data-selected={project == this.state.filter.project && id == this.state.filter.type} 
                                onPointerUp={this.onSelectFilter.bind(this, project, id, null)}>
                                {text + " (" + group.cards.length + ")"}
                            </div>
                        </div>
                        {group.isExpanded &&
                            <div>
                                {group.cards.map((card, index) => {
                                    return (
                                        <div 
                                            key={index}
                                            className="explore_card"
                                            data-selected={card == this.state.filter.card} 
                                            onPointerUp={this.onSelectFilter.bind(this, project, null, card)}>
                                            {this.renderCard(card)}
                                        </div>
                                    );
                                })}
                            </div>
                        }
                    </div>
                );
            }
        };
        return (
            <div className="explore_search_list">
                {this.state.searchList.projects.map((project, index) => {
                    return (
                        <div 
                            key={index}
                            className="explore_search_list_project">
                            <div>
                                <i 
                                    className={project.isExpanded ? "up" : "down"}
                                    onPointerUp={() => {project.isExpanded = !project.isExpanded; this.forceUpdate();}}/>
                                <div
                                    data-selected={project == this.state.filter.project && this.state.filter.type == null && this.state.filter.card == null} 
                                    onPointerUp={this.onSelectFilter.bind(this, project, null, null)}>
                                    {this.renderHighlightedText(project.name)}
                                </div>
                            </div>
                            {project.isExpanded && renderList(project, project.level1Cards, "Nivå 1", this.FILTER_LEVEL_1)}
                            {project.isExpanded && renderList(project, project.level2Cards, "Nivå 2", this.FILTER_LEVEL_2)}
                            {project.isExpanded && renderList(project, project.taggedCards, "Tag", this.FILTER_TAGGED)}
                            {project.isExpanded && renderList(project, project.otherCards, "Övriga", this.FILTER_OTHERS)}
                        </div>
                    );
                })}
            </div>
        );
    }

    renderTreeView(treeView, selected) {
        var renderChildren = (children) => {
            return children.map((element, index) => {
                return (
                    <div 
                        key={index}
                        data-collapsed={selected == element.node}
                        className="explore_card_node">
                        <div className="explore_card_node_label">
                            {element.children.length > 0 &&
                                <i 
                                    className={selected == element.node ? "down" : "up"}
                                    onPointerUp={this.onExpandClicked.bind(this)}/>
                            }
                            <div
                                data-selected={selected == element.node}                                
                                className="explore_card">
                                {this.renderCard(element.node)}
                            </div>
                        </div>
                        <div className="explore_card_children">
                            {renderChildren(element.children)}
                        </div>
                    </div>
                );
            });
        };
        return renderChildren(treeView.children);
    }

    renderDetailsList() {
        var cardList = [];
        if(this.state.filter.project) {
            if(this.state.filter.card) {
                cardList.push(this.state.filter.card);
            } else {
                var project = this.state.filter.project;
                cardList = project.cards;
                switch(this.state.filter.type) {
                    case this.FILTER_LEVEL_1:
                        cardList = project.level1Cards.cards;
                    break;
                    case this.FILTER_LEVEL_2:
                        cardList = project.level2Cards.cards;
                    break;
                    case this.FILTER_TAGGED:
                        cardList = project.taggedCards.cards;
                    break;
                    case this.FILTER_OTHERS:
                        cardList = project.otherCards.cards;
                    break;
                }
            }
        } else {
            cardList = this.state.searchList.projects.flatMap((project, index) => {
                var cards = project.cards;
                return cards;
            });
        }

        var cards = cardList.map((element, index) => {
            return (
                <div key={index}>
                    {this.renderTreeView(this.createTreeView(element), element)}
                </div>
            );
            /*var segment = (card, segIndex) => {
                return (
                    <div 
                        key={segIndex}
                        title={this.cardName(card)}>
                        {this.cardName(card)}
                    </div>
                );
            };
            var name = this.cardName(element);
            var path = element.getPath();
            path = path.slice(0, path.length - 1).map(segment);
            //path.unshift(segment(element.project, 99999));
            return (
                <div 
                    key={index}
                    className="explore_card">
                    <div className="explore_card_path">{path}</div>
                    <div>{name}</div>
                </div>
            );*/
        });
        return (
            <div className="explore_details_list">
                {this.renderWorkModeMenu()}
                {cards}
            </div>
        );
    }

    renderTreeView2(treeView, selectedList) {
        var isSelected = (node) => {
            return selectedList == null ? false : selectedList.indexOf(node) != -1;
        };
        var hasSelectedChildren = (element) => {
            if(element && element.children) {
                for(var i=0; i<element.children.length; ++i) {
                    var child = element.children[i];
                    if(isSelected(child.node) || hasSelectedChildren(child)) {
                        return true;
                    }
                }
            }
            return false;
        };
        var renderChildren = (children) => {            
            return children.map((element, index) => {
                var selected = isSelected(element.node);
                var selectedChildren = hasSelectedChildren(element);
                if(this.state.showBranches || selected || selectedChildren) {
                    return (                    
                        <div 
                            key={index}
                            data-collapsed={!selectedChildren}
                            className="explore_card_node">
                            <div className="explore_card_node_label">
                                {(element.children.length > 0 && (this.state.showBranches || selectedChildren)) &&
                                    <i 
                                        className={selectedChildren ? "up" : "down"}
                                        onPointerUp={this.onExpandClicked.bind(this)}/>
                                }
                                <div
                                    data-selected={selected}
                                    className="explore_card">
                                    {this.renderCard(element.node)}
                                </div>
                            </div>
                            <div className="explore_card_children">
                                {renderChildren(element.children)}
                            </div>
                        </div>
                    );
                }
            });
        };
        return renderChildren(treeView.children);
    }

    renderDetailsList2() {
        var cardList = [];
        var projects = [];
        if(this.state.filter.project) {
            var project = this.state.filter.project;
            if(this.state.filter.card) {
                cardList.push(this.state.filter.card);
            } else {
                cardList = project.cards;
                switch(this.state.filter.type) {
                    case this.FILTER_LEVEL_1:
                        cardList = project.level1Cards.cards;
                    break;
                    case this.FILTER_LEVEL_2:
                        cardList = project.level2Cards.cards;
                    break;
                    case this.FILTER_TAGGED:
                        cardList = project.taggedCards.cards;
                    break;
                    case this.FILTER_OTHERS:
                        cardList = project.otherCards.cards;
                    break;
                }
            }
            projects.push(
                <div 
                    key="0"
                    data-collapsed={!project.isExpandedDetails}
                    className="explore_card_node">
                    <div className="explore_card_node_label">
                        <i 
                            className={project.isExpandedDetails ? "up" : "down"}
                            onPointerUp={this.onExpandClicked.bind(this)}/>
                        <div
                            data-selected={false}
                            className="explore_card">
                            {project.name}
                        </div>
                    </div>
                    <div className="explore_card_children">
                        {project.topNodes.map((element, index) => {
                            return (
                                <div key={index}>
                                    {this.renderTreeView2(this.createTreeView(element), cardList)}
                                </div>
                            );
                        })}
                    </div>
                </div>
            );
        } else {
            projects = this.state.searchList.projects.map((project, index) => {
                var selectedList = this.state.activeSearch.length > 0 ? project.cards : null;
                return (
                    <div 
                        key={index}
                        data-collapsed={!project.isExpandedDetails}
                        className="explore_card_node">
                        <div className="explore_card_node_label">
                            <i 
                                className={project.isExpandedDetails ? "up" : "down"}
                                onPointerUp={this.onExpandClicked.bind(this)}/>
                            <div
                                data-selected={false}
                                className="explore_card">
                                {project.name}
                            </div>
                        </div>
                        <div className="explore_card_children">
                            {project.topNodes.map((element, index) => {
                                return (
                                    <div key={index}>
                                        {this.renderTreeView2(this.createTreeView(element), selectedList)}
                                    </div>
                                );
                            })}
                        </div> 
                    </div>
                );
            });
        }
        
        return (
            <div className="explore_details_list">                
                {this.renderWorkModeMenu()}                
                {projects}
            </div>
        );
    }

    renderWorkModeMenu() {
        return (
            <div className="explore_work_mode">
                <div
                    onPointerUp={() => this.setState({mode: 0})}
                    style={{background:"#99f"}}>
                    {Constants.ICON_TREE}
                </div>
                <div
                    onPointerUp={() => this.setState({mode: 1})}
                    style={{background:"#f9f"}}>
                    {Constants.ICON_TREE}
                </div>
                {this.state.mode == 1 &&
                    <div onPointerUp={() => this.setState({showBranches: !this.state.showBranches})}>
                        {this.state.showBranches ? Constants.ICON_EYE : Constants.ICON_EYE_SLASH}
                    </div>
                }
            </div>
        );
    }

    render() {        
        return (
            <div className="explore">
                <div>
                    <div className="explore_search">
                        <input 
                            type="text"
                            className="rounded"
                            value={this.state.search}
                            onKeyUp={this.onSearchKeyUp.bind(this)}
                            onChange={this.onSearchChanged.bind(this)}/>
                        <Button 
                            css="explore_search_button"
                            text="Sök"
                            onClick={this.onSearchClicked.bind(this)}/>
                    </div>
                    {this.renderSearchList()}
                </div>                
                {this.state.mode == 0 &&
                    this.renderDetailsList()
                }
                {this.state.mode == 1 &&
                    this.renderDetailsList2()
                }
            </div>
        );
    }
	
}

export default Explore;