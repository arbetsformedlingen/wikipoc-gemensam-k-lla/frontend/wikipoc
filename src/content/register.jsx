import React from 'react';
import ReactDOM from 'react-dom';
import Button from './../control/button.jsx';
import InputField from './../control/input_field.jsx';
import Constants from './../context/constants.jsx';
import EventDispatcher from './../context/event_dispatcher.jsx';
import App from './../context/app.jsx';

class Register extends React.Component { 

	constructor() {
        super();
        this.name = null;
        this.org = null;
        this.email = null;
        this.password = null;
    }
    
    onRegisterClicked() {
        var user = {
            name: this.name,
            org: this.org,
            email: this.email,
            password: this.password,
        };
        localStorage.setItem("wiki_user", JSON.stringify(user));

        // TODO: veryify succesfull registration before calling these..
        App.onUserLoggedIn(user);
        EventDispatcher.fire(Constants.EVENT_SET_PAGE, Constants.PAGE_REGISTER_STATUS);
    }

    render() {        
        return (
            <div className="register">
                <div>
                    <div className="register_header">
                        Fyll i dina uppgifter för att komma igång
                    </div>
                    <InputField 
                        title="Ditt namn"
                        onValueChange={(v) => this.name = v}/>
                    <InputField 
                        title="Organisation"
                        onValueChange={(v) => this.org = v}/>
                    <InputField 
                        title="Epost"
                        onValueChange={(v) => this.email = v}/>
                    <InputField 
                        title="Lösenord"
                        isPassword={true}
                        onValueChange={(v) => this.password = v}/>
                    <Button 
                        text="Skapa nytt konto"
                        onClick={this.onRegisterClicked.bind(this)}/>
                </div>
            </div>
        );
    }
	
}

export default Register;