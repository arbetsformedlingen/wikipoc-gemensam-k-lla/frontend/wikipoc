import React from 'react';
import ReactDOM from 'react-dom';
import Link from './../control/link.jsx';
import App from './../context/app.jsx';
import Constants from './../context/constants.jsx';
import EventDispatcher from './../context/event_dispatcher.jsx';

class Start extends React.Component { 

	constructor() {
        super();
    }

    goto(page) {
        if(App.user) {
            EventDispatcher.fire(Constants.EVENT_SET_PAGE, page);
        } else {
            App.targetPage = page;
            EventDispatcher.fire(Constants.EVENT_SET_PAGE, Constants.PAGE_LOGIN);
        }
    }

    onPrepareClicked() {
        this.goto(Constants.PAGE_PROJECT);
    }

    onJoinClicked() {
        this.goto(Constants.PAGE_PROJECT);
    }

    onExploreClicked() {
        this.goto(Constants.PAGE_EXPLORE);
    }

    onUploadClicked() {
        this.goto(Constants.PAGE_MATERIAL);
    }

    renderInfo() {
        return (
            <div className="start_info">
                <h3>Skapa, dela och samarbeta med WikiPoC</h3>
                <div>Med WikiPoC kan du enkelt samla in, granska och använda material för att hitta kvalitetssäkrade översättningsnycklar mellan arbetsmarknad och utbildnings..</div>
            </div>
        );
    }

    renderCard(icon, title, info, onClick) {
        return (
            <div 
                className="card"
                onPointerUp={onClick}>
                <div>{icon}</div>
                <h3>{title}</h3>
                {info &&
                    <div>{info}</div>
                }
            </div>
        );
    }

    renderOptions() {
        return (
            <div className="start_options">
                <h4>Vad vill du göra?</h4>
                <div>
                    {this.renderCard(Constants.ICON_LIGHTBULB, "Förbereda projekt", "Sätt upp material snabbt", this.onPrepareClicked.bind(this))}
                    {this.renderCard(Constants.ICON_PEN, "Delta i ett projekt", "Välj det projekt du vill delta i", this.onJoinClicked.bind(this))}
                    {this.renderCard(Constants.ICON_MAGNIFYING_GLAS, "Utforska och använd befintligt material", null, this.onExploreClicked.bind(this))}
                </div>
                <Link 
                    css="link start_upload_link"
                    text="Ladda upp befintligt material"
                    onClick={this.onUploadClicked.bind(this)}/>
            </div>
        );
    }

    render() {        
        return (
            <div className="start">
                <div>
                    {this.renderInfo()}
                    {this.renderOptions()}
                </div>
            </div>
        );
    }
	
}

export default Start;