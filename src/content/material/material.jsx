import React from 'react';
import Constants from './../../context/constants.jsx';
import App from './../../context/app.jsx';
import Explore from './explore.jsx';
import View from './view.jsx';

class Material extends React.Component { 

	constructor() {
        super();
    }

    render() {
        return (
            <div className="material">
                {(App.page == Constants.PAGE_MATERIAL || App.page == Constants.PAGE_MATERIAL_EXPLORE) &&
                    <Explore />
                }
                {App.page == Constants.PAGE_MATERIAL_VIEW &&
                    <View />
                }
            </div>
        );
    }
	
}

export default Material;