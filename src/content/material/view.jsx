import React from 'react';
import Button from './../../control/button.jsx';
import App from './../../context/app.jsx';
import Constants from './../../context/constants.jsx';
import EventDispatcher from './../../context/event_dispatcher.jsx';
import Card from './../../component/card.jsx';

class View extends React.Component { 

	constructor(props) {
        super(props);

        var sheets = App.file.data.worksheets;
        var tabs = sheets.map((sheet, index) => {
            var rowOptions = [];
            var rows = [];
            sheet.eachRow({ includeEmpty: false }, (row, rowNumber) => {
                var column = [];
                var number = -1;
                var count = 1;
                var pattern = "";
                for(var i=0; i<sheet.actualColumnCount; ++i) {
                    var cell = row._cells[i];
                    var data = null;
                    if(cell == null || cell == undefined) {
                        data = {
                            width: 20,
                            height: 20,
                            value: "",
                        };
                    } else {
                        data = {
                            border: cell.style ? cell.style.border : null,
                            rowSpan: 0,
                            colSpan: 0,
                            width: cell._column.width,
                            height: row.height,
                            value: cell._value._master ? null : this.getCellValue(cell),
                            hasMaster: cell._value._master != null,
                            actualRowNumber: cell._row._number,
                            actualColumnNumber: cell._column._number,
                            columnNumber:  cell._value._master ? cell._value._master._column._number : cell._column._number,
                            rowNumber: cell._value._master ? cell._value._master._row._number : cell._row._number,
                        };
                    }
                    // update masters row / col-span
                    if(cell && cell._value._master) {
                        if(cell._value._master._row._number == cell._row._number) {
                            var master = column.find((x) => {
                                return x.actualColumnNumber == cell._value._master._column._number;
                            });
                            master.colSpan++;
                        } else {
                            var master = rows.find((x) => {
                                return x.columns.find((y) => {
                                    return y.actualRowNumber == cell._value._master._row._number &&
                                           y.actualColumnNumber == cell._value._master._column._number;
                                });
                            });
                            master.rowSpan++;
                        }
                    }
                    // generate the row specific pattern to allow selections
                    if(cell) {
                        if(cell._value._master && cell._value._master._row._number == row._number) {
                            count++;
                        } else {
                            pattern += "" + count;
                            count = 1;
                        }
                    }
                    if(number == -1) { 
                        number = data.rowNumber;
                    }
                    column.push(data);
                }




                pattern += "" + count;
                if(rowOptions.length == 0 || rowOptions[rowOptions.length - 1].rowNumber != number) {
                    rowOptions.push({
                        pattern: pattern,
                        rowNumber: number,
                        value: "0",
                    });
                }
                rows.push({
                    rowNumber: rowNumber,
                    columns: column,
                });
            });
            // find multirow patterns
            for(var i=1; i<rowOptions.length; ++i) {
                var e = rows.find((x) => {
                    return x.rowNumber == rowOptions[i].rowNumber;
                });
                if(rowOptions[i].rowNumber != rowOptions[i - 1].rowNumber + 1) {
                    rowOptions[i - 1].pattern = "multirow";
                } else if(i == rowOptions.length - 1 && rowOptions[i].rowNumber != sheet.actualRowCount) {
                    rowOptions[i].pattern = "multirow";
                } else {
                    if(e) {
                        for(var j=0; j<e.columns.length; ++j) {
                            if(e.columns[j].rowNumber == rowOptions[i - 1].rowNumber) {
                                rowOptions[i - 1].pattern = "multirow";
                            }
                        }
                    }
                }
                if(e) {
                    for(var j=0; j<e.columns.length; ++j) {
                        if(e.columns[j].rowNumber != rowOptions[i].rowNumber) {
                            rowOptions[i].pattern = "multirow";
                        }
                    }
                }
            }
            return {
                name: sheet.name,
                index: index,
                sheet: sheet,
                columnOptions: new Array(sheet.actualColumnCount).fill("0"),
                rowOptions: rowOptions,
                rows: rows,
            };
        });

        this.state = {
            sheets: sheets,
            activeSheet: 0,
            tabs: tabs,
        };
    }

    onTabClicked(sheetIndex) {
        this.setState({activeSheet: sheetIndex});
    }

    onColumnOptionChanged(tab, index, e) {
        tab.columnOptions[index] = e.target.value;
        this.forceUpdate();
    }
    
    onRowOptionChanged(tab, rowOption, e) {
        var value = e.target.value;
        for(var i=0; i<tab.rowOptions.length; ++i) {
            if(tab.rowOptions[i].rowNumber >= rowOption.rowNumber && tab.rowOptions[i].pattern == rowOption.pattern) {
                tab.rowOptions[i].value = value;
            }
        }
        this.forceUpdate();
    }

    getCellValue(cell) {
        if(cell._value.model.value && cell._value.model.value.richText) {
            var value = "";
            for(var i=0; i<cell._value.model.value.richText.length; ++i) {
                value += cell._value.model.value.richText[i].text + " ";
            }
            return value.trim();
        } 
        if(cell.value && cell.value.hyperlink) {
            return cell.value.hyperlink.trim();
        }
        if(cell.value && typeof(cell.value) == "string") {
            return cell.value;
        }
        return "";
    }

    onImportClicked() {
        // TODO: go over all tabs
        var tab = this.state.tabs[this.state.activeSheet];
        var previousOptionValue = null;
        var tags = null;
        var rootOffset = 0;
        var childOffset = 0;
        var data = [];
        var parent = null;
        var subParent = null;
        var id = 1;
        var getTags = (columnNumber) => {
            var t = tags ? tags.find((x) => {
                return x.columnNumber == columnNumber;
            }) : null;
            return t ? t.values : null;
        };
        var parseLevel1 = (option, rows) => {
            var source = rows[0].columns[0];
            if(source.value && source.value.trim().length > 0 && tab.columnOptions[0] != "1") {
                var card = new Card();
                card.addConnectionNode("Related");
                card.description = source.value;
                card.y = rootOffset;
                card.xlslId = source.actualRowNumber + "#" + source.actualColumnNumber;
                card.id = id++;
                rootOffset += 200.0;
                parent = card;
                data.push(card);
            } else {
                parent = null;
            }
        };
        var parseLevel2 = (option, rows) => {
            // create all the cards
            rows.forEach((row) => {
                row.columns.filter((x) => { 
                    return !x.hasMaster && x.value && x.value.trim().length > 0 && tab.columnOptions[x.actualColumnNumber - 1] != "1"; 
                }).forEach((column) => {
                    var cardTags = getTags(column.actualColumnNumber);
                    var card = new Card();
                    card.id = id++;
                    card.description = column.value;
                    card.xlslId = column.actualRowNumber + "#" + column.actualColumnNumber;
                    card.xlslRow = column.actualRowNumber;
                    card.xlslColumn = column.actualColumnNumber;
                    if(cardTags) {
                        for(var i=0; i<cardTags.length; ++i) {
                            card.tags.push(cardTags[i]);
                        }
                    }
                    data.push(card);
                });
            });
            // connect and position cards
            rows.forEach((row) => {
                row.columns.forEach((column) => {
                    var id = column.rowNumber + "#" + column.columnNumber;
                    var card = data.find((x) => {
                        return x.xlslId == id;
                    });
                    if(card) {
                        // if this card is in column 1 (index 0), connect to the current parent
                        // and set this as the current sub parent
                        if(card.isXlslProcessed == null && card.xlslColumn == 1) {
                            card.x = 300.0;
                            card.y = childOffset;
                            card.addConnectionNode("Related");
                            childOffset += 100.0;
                            if(parent) {
                                parent.getConnection("Related").connect(card);
                            }
                            subParent = card;
                        } else {
                            // this should be a level 2 card, try and connect to previous sub parent
                            if(card.isXlslProcessed == null) {
                                card.x = 600.0;
                                card.y = childOffset;
                                childOffset += 100.0;
                            }
                            if(subParent != card) {
                                subParent.getConnection("Related").connect(card);
                            }
                        }
                        card.isXlslProcessed = true;
                    }
                });
            });
        };
        var parseTags = (option, rows) => {
            if(option.value != previousOptionValue) {
                tags = null;
            }
            if(tags == null) {
                tags = [];
            }
            // merge tags
            for(var j=0; j<rows.length; ++j) {
                var row = rows[j];
                for(var k=0; k<row.columns.length; ++k) {
                    var column = row.columns[k];
                    if(column.value && column.value.trim().length > 0) {
                        for(var l=0; l<column.colSpan + 1; ++l) {
                            var tag = tags.find((x) => {
                                return x.columnNumber == column.actualColumnNumber + l;
                            });
                            if(tag == null) {
                                tag = {
                                    columnNumber: column.actualColumnNumber + l,
                                    values: []
                                };
                                tags.push(tag);
                            }
                            tag.values.push(column.value.trim());
                        }
                    }
                }
            }
        };
        // parse cells and generate cards
        for(var i=0; i<tab.rowOptions.length; ++i) {
            var option = tab.rowOptions[i];
            var nextOption = i == tab.rowOptions.length - 1 ? null : tab.rowOptions[i + 1];
            var rows = tab.rows.filter((row) => {
                if(nextOption == null) {
                    return row.rowNumber >= option.rowNumber;
                }
                return row.rowNumber >= option.rowNumber && row.rowNumber < nextOption.rowNumber;
            });
            if(option.value == "1") {
                parseLevel1(option, rows);
            } else if(option.value == "2") {
                parseLevel2(option, rows);
            } else if(option.value == "3") {
                parseTags(option, rows);
            }
            previousOptionValue = option.value;
        }
        App.project = App.createProject();
        App.project.cards = data;
        App.project.nextId = id;
        App.myProjects.push(App.project);
        EventDispatcher.fire(Constants.EVENT_SET_PAGE, Constants.PAGE_PROJECT_EDIT);
    }

    onResetClicked() {
        for(var i=0; i<this.state.tabs.length; ++i) {
            var tab = this.state.tabs[i];
            for(var j=0; j<tab.columnOptions.length; ++j) {
                tab.columnOptions[j] = "0";
            }
            for(var j=0; j<tab.rowOptions.length; ++j) {
                tab.rowOptions[j] = "0";
            }
        }
        this.forceUpdate();
    }

    renderWorksheet(tab) {
        var rows = []
        // column options
        var options = [
            <div
                key="dummy" 
                className="material_row_option"/>
        ];
        for(var i=0; i<tab.columnOptions.length; ++i) {
            var column = tab.rows[0].columns[i];
            var style = {
                width: "" + (column.width / 2) + "em",
            };
            options.push(
                <div
                    key={i + "_" + tab.index}
                    style={style}
                    className="material_column_option">
                    <select 
                        value={tab.columnOptions[i]}
                        onChange={this.onColumnOptionChanged.bind(this, tab, i)}>
                        <option value="0">Inkludera</option>
                        <option value="1">Exkludera</option>
                    </select>
                </div>
            );
        }
        rows.push(
            <div 
                key="options"
                className="material_row">
                {options}
            </div>
        );
        rows.push(...tab.rows.map((row, rowIndex) => {
            var columns = [];
            var rowOption = null;
            for(var i=tab.rowOptions.length -1; i>=0; --i) {
                if(tab.rowOptions[i].rowNumber <= row.rowNumber) {
                    rowOption = tab.rowOptions[i];
                    break;
                }
            }
            if(rowOption && rowOption.rowNumber == row.rowNumber) {
                columns.push(
                    <div 
                        key={999}
                        className="material_row_option">
                        <select
                            value={rowOption.value}
                            onChange={this.onRowOptionChanged.bind(this, tab, rowOption)}>
                            <option value="0">-- Välj</option>
                            <option value="1">Nivå 1</option>
                            <option value="2">Nivå 2</option>
                            <option value="3">Tag</option>
                            <option value="4">Exkludera</option>
                        </select>
                    </div>
                );
            } else {
                columns.push(
                    <div 
                        key={999}
                        className="material_row_option">
                    </div>
                );
            }
            columns.push(...row.columns.map((column, columnIndex) => {
                var color = null;
                if(rowOption) {
                    if(rowOption.value == "1") {
                        color = "#BDCDD6";
                    } else if(rowOption.value == "2") {
                        color = "#EEE9DA";
                    } else if(rowOption.value == "3") {
                        color = "#93BFCF";
                    }
                }
                if(tab.columnOptions[columnIndex] == "1") {
                    // exlude
                    color = null;
                }
                var style = {
                    width: "" + (column.width / 2) + "em",
                    height: "" + (column.height / 8) + "em",
                    background: color,
                };
                if(column.border) {
                    var shadows = [];
                    if(column.border.right) {
                        shadows.push("1px 0 0 0 #ccc");
                    }
                    if(column.border.bottom) {
                        shadows.push("0 1px 0 0 #ccc");
                    }
                    if(column.border.left) {
                        shadows.push("1px 0 0 0 #ccc inset");
                    }
                    if(column.border.top) {
                        shadows.push("0 1px 0 0 #ccc inset");
                    }
                    if(shadows.length > 0) {
                        style.boxShadow = shadows.join(",");
                    }
                }
                return (
                    <div 
                        key={columnIndex}
                        style={style}
                        className="material_cell">
                        {column.value}
                    </div>
                );
            }));
            return (
                <div 
                    key={rowIndex}
                    className="material_row">
                    {columns}
                </div>
            );
        }));
        return rows;   
    }

    renderHeader() {
        return (
            <div className="material_header">
                <div className="icon">{Constants.ICON_SVG_EXCEL}</div>
                <h1>{App.file.name}</h1>
            </div>
        );
    }

    renderTabs() {
        return (
            <div className="tab_container">
                <div className="tabs">
                    {this.state.sheets.map((sheet, index) => {
                        return (
                            <div 
                                key={index}
                                className={"tab_item" + (this.state.activeSheet == index ? " tab_item_active" : "")}
                                onPointerUp={this.onTabClicked.bind(this, index)}>
                                {sheet.name}
                            </div>
                        );
                    })}
                </div>
            </div>
        );
    }

    renderSheet() {
        return (
            <div className="sheet_container">
                <div className="sheet">
                    {this.renderWorksheet(this.state.tabs[this.state.activeSheet])}
                </div>
            </div>
        );
    }
    
    render() {        
        return (
            <div className="material_view">
                {this.renderHeader()}
                <div className="material_menu">
                    <Button 
                        text="Importera"
                        onClick={this.onImportClicked.bind(this)}/>
                    <Button 
                        text="Återställ"
                        onClick={this.onResetClicked.bind(this)}/>
                </div>
                <div className="material_content_relative">
                    <div className="material_content">
                        {this.renderTabs()}
                        {this.renderSheet()}
                    </div>
                </div>
            </div>
        );
    }
	
}

export default View;