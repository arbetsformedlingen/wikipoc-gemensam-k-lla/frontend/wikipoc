import React from 'react';
import ExcelLib from 'exceljs';
import App from './../../context/app.jsx';
import Constants from './../../context/constants.jsx';
import EventDispatcher from './../../context/event_dispatcher.jsx';
import Button from './../../control/button.jsx';

class Explore extends React.Component { 

	constructor() {
        super();
        this.state = {
            isImporting: false,
        }
    }

    readFile(file) {

        // TODO: correctly validate file type

        var workbook = new ExcelLib.Workbook();
        var reader = new FileReader();
        reader.readAsArrayBuffer(file);
        reader.onload = () => {
            workbook.xlsx.load(reader.result).then((workbook) => {
                var newFile = {
                    type: Constants.FILE_TYPE_XLSX,
                    name: file.name,
                    data: workbook,
                };
                App.myFiles.push(newFile);
                // go directly to the uploaded file
                App.file = newFile;
                EventDispatcher.fire(Constants.EVENT_SET_PAGE, Constants.PAGE_MATERIAL_VIEW);
            });
        };
    }

    onChooseFileClicked() {
        var filePicker = document.getElementById("file_picker");
        filePicker.click();
    }

    onFilePicker(e) {
        var file = e.target.files[0];      
        this.setState({
            isImporting: true,
        }, () => {
            this.readFile(file);
        });
    }

    onFileClicked(file) {
        App.file = file;
        EventDispatcher.fire(Constants.EVENT_SET_PAGE, Constants.PAGE_MATERIAL_VIEW);
    }

    renderIcon(type) {
        if(type == Constants.FILE_TYPE_XLSX) {
            return (
                <div className="card_icon">{Constants.ICON_SVG_EXCEL}</div>
            );
        }
    }

    render() {
        var files = App.myFiles.map((file, i) => {
            return (
                <div
                    className="card" 
                    key={i}
                    onPointerUp={this.onFileClicked.bind(this, file)}>
                    {this.renderIcon(file.type)}
                    <div>{file.name}</div>
                </div>
            );
        });      
        return (
            <div className="material_explore">
                <h2 className="content_title">
                    Material
                </h2>
                <div className="project_explore">
                    {files}
                </div>
                <Button
                    text="Ladda upp"
                    onClick={this.onChooseFileClicked.bind(this)}/>
                <input 
                    id="file_picker" 
                    className="material_choose_button" 
                    type="file"
                    accept=".xlsx"
                    onChange={this.onFilePicker.bind(this)} />
            </div>
        );
    }
	
}

export default Explore;