import React from 'react';
import EditCard from './edit_card.jsx';
import Whiteboard from './../../component/whiteboard.jsx';
import EventDispatcher from './../../context/event_dispatcher.jsx';
import Constants from './../../context/constants.jsx';
import App from './../../context/app.jsx';
import Util from './../../context/util.jsx';
import Card from './../../component/card.jsx';

class ModeWhiteboard extends React.Component { 

	constructor() {
        super();
        this.whiteboard = new Whiteboard();
        this.state = {
            taskValue: "",
            editCard: null,
        };
        // bindings
        this.boundEditCard = this.onEditCard.bind(this);
        this.boundDeleteCard = this.onDeleteCard.bind(this);        
    }

    componentDidMount() {
        // setup whiteboard
        var c = document.getElementById("whiteboard_container");
        var w = document.getElementById("whiteboard");
        // init rendering
        this.whiteboard.onInit(w, c.clientWidth, c.clientHeight);
        this.whiteboard.onBindEvents(c);
        if(App.project.cards) {
            for(var i=0; i<App.project.cards.length; ++i) {
                this.whiteboard.add(App.project.cards[i]);
            }
        }
        // events
        EventDispatcher.add(this.boundEditCard, Constants.EVENT_EDIT_CARD);
        EventDispatcher.add(this.boundDeleteCard, Constants.EVENT_DELETE_CARD);
    }

    componentWillUnmount() {
        this.whiteboard.onClose();
        this.whiteboard.onUnbindEvents();
        // events
        EventDispatcher.remove(this.boundEditCard);
        EventDispatcher.remove(this.boundDeleteCard);
    }

    onEditCard(card) {
        this.whiteboard.setEnableEvents(card == null);
        this.whiteboard.activeCard = card;
        this.setState({editCard: card});
    }

    onDeleteCard(card) {
        if(card.onDelete) {
            card.onDelete();
        }
        // remove from project
        var index = App.project.cards.indexOf(card);
        App.project.cards.splice(index, 1);
    }

    onTaskValueChanged(e) {
        this.setState({taskValue: e.target.value});
    }

    onTaskValueKeyDown(e) {
        if(e.key == "Enter" && this.state.taskValue.length > 0) {
            var location = Util.vec2(this.whiteboard.width * 0.5, this.whiteboard.height * 0.5);
            var card = new Card();
            location.x -= card.width * 0.5;
            location.y -= card.height * 0.5;
            location = this.whiteboard.toWorldSpace(location);
            card.x = location.x;
            card.y = location.y;
            card.name = this.state.taskValue;
            card.id = App.project.nextId++;
            this.whiteboard.add(card);
            this.setState({taskValue: ""});
            // save card in project
            if(App.project.cards == null) {
                App.project.cards = [];
            }
            App.project.cards.push(card);
        }
    }

    render() {        
        return (
            <div className="project_view">
                <div className="project_view_input_header">
                    <input 
                        type="text"
                        className="rounded"
                        placeholder="Skriv in arbetsuppgift"
                        value={this.state.taskValue}
                        onChange={this.onTaskValueChanged.bind(this)}
                        onKeyDown={this.onTaskValueKeyDown.bind(this)}/>
                </div>
                <div 
                    className="project_whiteboard"
                    id="whiteboard_container">
                    <canvas id="whiteboard"/>
                    {this.state.editCard &&
                        <EditCard card={this.state.editCard}/>
                    }
                </div>
            </div>
        );       
    }
	
}

export default ModeWhiteboard;