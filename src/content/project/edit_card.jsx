import React from 'react';
import Util from './../../context/util.jsx';
import Constants from './../../context/constants.jsx';
import EventDispatcher from './../../context/event_dispatcher.jsx';
import App from './../../context/app.jsx';
import Button from './../../control/button.jsx';

class EditCard extends React.Component { 

	constructor(props) {
        super(props);
        var card = props.card;
        // constants
        this.CONTENT_INFO = 0;
        this.CONTENT_CONNECTIONS = 1;
        this.CONTENT_TAGS = 2;
        this.CONTENT_SETTINGS = 3;
        this.state = {
            content: this.CONTENT_INFO,
            name: card.name,
            description: card.description ? card.description : "",
            filter: "",
        };
        if(props.modeGrid == null) {
            // get location for window to appear, correct the location if to close to edges
            var location = Util.vec2(card.x + card.width + 10, card.y);
            location = card.whiteboard.toScreenSpace(location);
            if(location.x > card.whiteboard.width - 250) {
                location.x = card.whiteboard.width - 300;
            }
            if(location.y > card.whiteboard.height - 340) {
                location.y = card.whiteboard.height - 340;
            }
            this.location = location;
        } else {
            this.location = Util.vec2(0, 0);
        }
    }

    onNameChanged(e) {
        // NOTE: this should work just as the "big" input field, when the user first created the card
        //       with autocomplete and searching for tags etc...
        this.setState({name: e.target.value});
    }

    onDescriptionChanged(e) {
        this.setState({description: e.target.value});
    }

    onSettingsClicked() {
        this.setState({content: this.CONTENT_SETTINGS});
    }

    onSaveClicked() {
        var card = this.props.card;
        card.setName(this.state.name);
        card.setDescription(this.state.description);
        EventDispatcher.fire(Constants.EVENT_EDIT_CARD, null);
    }

    onAbortClicked() {
        EventDispatcher.fire(Constants.EVENT_EDIT_CARD, null);
    }

    onDeleteClicked() {
        EventDispatcher.fire(Constants.EVENT_DELETE_CARD, this.props.card);
        EventDispatcher.fire(Constants.EVENT_EDIT_CARD, null);
    }

    onFilterChanged(e) {
        this.setState({filter: e.target.value});
    }

    onFilterKeyDown(e) {
        if(e.key == "Enter" && this.isNewTag()) {
            if(this.state.content == this.CONTENT_TAGS) {
                this.onAddTagClicked();
            } else {
                this.onAddConnectionClicked();
            }
        }
    }

    onConnectionClicked(connection) {
        var index = App.connectionTypes.indexOf(connection);
        if(this.props.card.getConnection(connection) == null) {
            this.props.card.addConnectionNode(connection);
            if(index == -1) {
                App.connectionTypes.push(connection);
            }
        } else {
            this.props.card.removeConnectionNode(connection);
            if(index != -1) {
                App.connectionTypes.splice(index, 1);
            }
        }
        this.forceUpdate();
    }

    onTagClicked(tag) {
        this.props.card.toggleTag(tag);
        this.forceUpdate();
    }

    onAddConnectionClicked() {
        App.connectionTypes.push(this.state.filter);
        if(this.props.card.getConnection(this.state.filter) == null) {
            this.props.card.addConnectionNode(this.state.filter);
        }
        this.setState({filter: ""});
    }

    onAddTagClicked() {        
        App.tags.push(this.state.filter);
        this.props.card.toggleTag(this.state.filter);
        this.setState({filter: ""});
    }

    isNewConnection() {
        if(this.state.filter.length > 0) {
            var query = this.state.filter.toLowerCase();
            return App.connectionTypes.map((x) => x.toLowerCase()).indexOf(query) < 0;
        }
        return false;
    }

    isNewTag() {
        if(this.state.filter.length > 0) {
            var query = this.state.filter.toLowerCase();
            return App.tags.map((x) => x.toLowerCase()).indexOf(query) < 0;
        }
        return false;
    }

    renderContent() {
        switch(this.state.content) {
            case this.CONTENT_INFO:
                return this.renderInfo();
            case this.CONTENT_CONNECTIONS:
                return this.renderConnetions();
            case this.CONTENT_TAGS:
                return this.renderTags();
        }
        return this.renderSettings();
    }

    renderInfo() {
        return (
            <div className="group">
                <input 
                    type="text" 
                    className="rounded"
                    placeholder="Namn"
                    value={this.state.name}
                    autoFocus
                    onChange={this.onNameChanged.bind(this)}/>
                <textarea 
                    rows="14"
                    className="rounded"
                    placeholder="Beskrivning"
                    value={this.state.description}
                    onChange={this.onDescriptionChanged.bind(this)}/>
            </div>
        );
    }

    renderConnetions() {
        var filter = (x) => {
            return this.state.filter.length > 0 ? x.toLowerCase().indexOf(this.state.filter.toLowerCase()) >= 0 : true;
        };
        var connections = App.connectionTypes.filter(filter).map((element, i) => {
            return (
                <Button 
                    key={i}                    
                    text={element}
                    css={this.props.card.getConnection(element) ? "selected_tag" : "unselected_tag"}
                    onClick={this.onConnectionClicked.bind(this, element)}/>
            );
        });
        return (
            <div className="group">
                <div className="tag_filter">
                    <input 
                        className="rounded"
                        type="text"
                        placeholder="Sök"
                        value={this.state.filter}
                        onKeyDown={this.onFilterKeyDown.bind(this)}
                        onChange={this.onFilterChanged.bind(this)}/>
                    {this.isNewConnection() &&
                        <Button
                            css="add_tag"                
                            text="Lägg till"
                            onClick={this.onAddConnectionClicked.bind(this)}/>
                    }
                </div>
                <div className="tag_list">
                    <div>
                        {connections}
                    </div>
                </div>
            </div>
        );
    }

    renderTags() {
        var filter = (x) => {
            return this.state.filter.length > 0 ? x.toLowerCase().indexOf(this.state.filter.toLowerCase()) >= 0 : true;
        };
        var tags = App.tags.filter(filter).map((element, i) => {
            return (
                <Button 
                    key={i}                    
                    text={element}
                    css={this.props.card.hasTag(element) ? "selected_tag" : "unselected_tag"}
                    onClick={this.onTagClicked.bind(this, element)}/>
            );
        });
        return (
            <div className="group">
                <div className="tag_filter">
                    <input 
                        className="rounded"
                        type="text"
                        placeholder="Sök"
                        value={this.state.filter}
                        onKeyDown={this.onFilterKeyDown.bind(this)}
                        onChange={this.onFilterChanged.bind(this)}/>
                    {this.isNewTag() &&
                        <Button             
                            css="add_tag"                
                            text="Lägg till"
                            onClick={this.onAddTagClicked.bind(this)}/>
                    }
                </div>
                <div className="tag_list">
                    <div>
                        {tags}
                    </div>
                </div>
            </div>
        );
    }

    renderSettings() {
        return (
            <div className="group">
                <Button 
                    text="Info"
                    onClick={() => this.setState({content: this.CONTENT_INFO, filter: ""})}/>
                <Button 
                    text="Kopplingar"
                    onClick={() => this.setState({content: this.CONTENT_CONNECTIONS, filter: ""})}/>
                <Button 
                    text="Taggar"
                    onClick={() => this.setState({content: this.CONTENT_TAGS, filter: ""})}/>
                <Button 
                    css="red_button"
                    text="Ta bort"
                    onClick={this.onDeleteClicked.bind(this)}/>
            </div>
        );
    }

    render() {
        var s = {
            left: "" + this.location.x + "px",
            top: "" + this.location.y + "px",
        };
        if(this.props.modeGrid) {
            s = {
                left: "calc(50% - 125px)",
                top: "calc(50% - 150px)",
                position: "fixed",
            };
        }
        return (
            <div 
                className="edit_card"
                style={s}>
                <div className="edit_card_menu">
                    <div>
                        <div onPointerUp={this.onSettingsClicked.bind(this)}>
                            {Constants.ICON_COGWHEEL}
                        </div>
                        <div onPointerUp={this.onSaveClicked.bind(this)}>
                            {Constants.ICON_SAVE}
                        </div>
                    </div>
                    <div onPointerUp={this.onAbortClicked.bind(this)}>
                        {Constants.ICON_CLEAR}
                    </div>
                </div>
                {this.renderContent()}
            </div>
        );       
    }
	
}

export default EditCard;