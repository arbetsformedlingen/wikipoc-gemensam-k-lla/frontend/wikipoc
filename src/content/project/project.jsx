import React from 'react';
import App from './../../context/app.jsx';
import Constants from './../../context/constants.jsx';
import Explore from './explore.jsx';
import View from './view.jsx';
import Edit from './edit.jsx';

class Project extends React.Component { 

	constructor() {
        super();       
    }

    render() {        
       return (
            <div className="project">
                {(App.page == Constants.PAGE_PROJECT_EXPLORE || App.page == Constants.PAGE_PROJECT) &&
                    <Explore />
                }
                {App.page == Constants.PAGE_PROJECT_VIEW &&
                    <View />
                }
                {App.page == Constants.PAGE_PROJECT_EDIT &&
                    <Edit />
                }
            </div>
        );
    }
	
}

export default Project;