import React from 'react';
import Button from './../../control/button.jsx';
import EventDispatcher from './../../context/event_dispatcher.jsx';
import Constants from './../../context/constants.jsx';
import App from './../../context/app.jsx';

class Edit extends React.Component { 

	constructor() {
        super();        
        this.state = {
            name: App.project ? App.project.name : "",
            description: App.project ? App.project.description : "",
        };
    }

    onNameChanged(e) {
        this.setState({name: e.target.value});
    }

    onDescriptionChanged(e) {
        this.setState({description: e.target.value});
    }

    onSaveClicked() {
        if(App.project == null) {
            App.project = App.createProject();
            App.myProjects.push(App.project);
            App.projects.push(App.project);
        }
        App.project.name = this.state.name;
        App.project.description = this.state.description;
        EventDispatcher.fire(Constants.EVENT_SET_PAGE, Constants.PAGE_PROJECT_VIEW);
    }

    onCancelClicked() {
        EventDispatcher.fire(Constants.EVENT_SET_PAGE, Constants.PAGE_PROJECT_EXPLORE);
    }

    render() {        
        return (
            <div>
                <h2 className="content_title">
                    Lägg till projektnamn
                </h2>
                <div className="project_edit">
                    <div>Yrkesprofil som ska beskrivas i projektet</div>
                    <div className="group">
                        <div>Exempel: Produktionstekniker</div>
                        <input 
                            type="text" 
                            className="rounded"
                            value={this.state.name}
                            onChange={this.onNameChanged.bind(this)}/>
                    </div>
                </div>
                <div className="project_edit">
                    <div>Avgränsning och precisering</div>
                    <div className="group">
                        <div>Exempel: Det är inte alla produktionstekniker vi pratar om nu. Bristen gäller för SMEs. Den produktionstekniker vi pratar om nu har inte kollegor med samma yrkesroll, dvs. man behöver bred kompetens eftersom man är den enda på plats. Vad kan det vara för företag? Man tillverkar små serier.Kanske max 40-50 anställda.</div>
                        <textarea 
                            rows="8"
                            className="rounded"
                            value={this.state.description}
                            onChange={this.onDescriptionChanged.bind(this)}/>
                    </div>
                </div>
                <Button
                    text="Spara"
                    css="project_edit_button"
                    onClick={this.onSaveClicked.bind(this)}/>
                <Button 
                    text="Avbryt"
                    css="project_edit_button"
                    onClick={this.onCancelClicked.bind(this)}/>
            </div>
        );
    }
	
}

export default Edit;