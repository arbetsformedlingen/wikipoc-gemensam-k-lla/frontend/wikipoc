import React from 'react';
import EventDispatcher from './../../context/event_dispatcher.jsx';
import Constants from './../../context/constants.jsx';
import App from './../../context/app.jsx';
import EditCard from './edit_card.jsx';

class ModeGrid4 extends React.Component { 

	constructor() {
        super();
        this.state = {
            editCard: null,
        };
        this.boundEditCard = this.onEditCardClicked.bind(this);
    }

    componentDidMount() {
        EventDispatcher.add(this.boundEditCard, Constants.EVENT_EDIT_CARD);
    }

    componentWillUnmount() {
        EventDispatcher.remove(this.boundEditCard);
    }

    onEditCardClicked(card) {
        this.setState({editCard: card});
    }

    renderGrid(cards) {
        var getTags = () => {
            var tags = [];
            if(cards) {
                for(var i=0; i<cards.length; ++i) {
                    var card = cards[i];                
                    for(var j=0; j<card.tags.length; ++j) {
                        if(tags.indexOf(card.tags[j]) == -1) {
                            tags.push(card.tags[j]);
                        }
                    }
                }
            }
            return tags;
        }
        var renderCard = (card, index) => {
            return (
                <div
                    className="mode_grid_column_card"
                    key={index}>                    
                    <div>{card.name}</div>
                    <div>{card.description}</div>
                    <div 
                        className="mode_grid_column_card_edit"
                        onPointerUp={() => this.onEditCardClicked(card)}>
                        {Constants.ICON_EDIT}
                    </div>
                    <div>#{card.id}</div>
                </div>
            );
        }
        var renderColumnCards = (tag, key) => {
            var columnCards = [];
            if(cards) {
                if(tag == "") {
                    columnCards = cards.filter((card) => card.tags.length == 0);
                } else {
                    columnCards = cards.filter((card) => card.tags.indexOf(tag) >= 0);
                }
            }
            return (
                <div                     
                    key={key}>
                        <div className="mode_grid_column_header">
                            {tag == "" ? "Kort" : "#" + tag}
                        </div>
                        {columnCards.map(renderCard)}
                </div>
            );
        }

        var tags = getTags();
        // make sure cards without tags gets included
        tags.unshift("");
        
        return (
            <div className="mode_grid_group">
                {tags.map(renderColumnCards)}
            </div>
        );
    }

    getConnectedCards(from, list) {
        for(var i=0; i<from.connections.length; ++i) {
            var connections = from.connections[i].connections;
            for(var j=0; j<connections.length; ++j) {                
                if(list.indexOf(connections[j]) == -1) {
                    list.push(connections[j]);
                    this.getConnectedCards(connections[j], list);
                }
            }
        }
    }

    renderGrids() {
        var roots = App.project.cards.filter((card) => card.numIncomingConnections() == 0);
        var grids = [];
        for(var i=0; i<roots.length; ++i) {
            var root = roots[i];
            var cards = [];
            this.getConnectedCards(root, cards);
            var name = root.name != null && root.name.length > 0 ? root.name : root.description;
            grids.push(
                <div key={i}>
                    <div><h2>{name}</h2></div>
                    {this.renderGrid(cards)}
                </div>
            );
        }
        return (
            <div className="mode_grid_groups">
                {grids}
            </div>
        );
    }

    render() {        
        return (
            <div className="project_view mode_grid_container">                
                {this.renderGrids()}
                {this.state.editCard != null &&
                    <EditCard 
                        card={this.state.editCard}
                        modeGrid={true}/>
                }
            </div>
        );       
    }
	
}

export default ModeGrid4;
