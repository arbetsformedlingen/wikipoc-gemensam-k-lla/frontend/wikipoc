import React from 'react';
import Button from './../../control/button.jsx';
import App from './../../context/app.jsx';
import EventDispatcher from './../../context/event_dispatcher.jsx';
import Constants from './../../context/constants.jsx';
import Rest from './../../context/rest.jsx';

class Explore extends React.Component { 

	constructor() {
        super();
    }

    onProjectClicked(project) {
        App.project = project;
        EventDispatcher.fire(Constants.EVENT_SET_PAGE, Constants.PAGE_PROJECT_VIEW);
    }

    onNewProjectClicked() {
        EventDispatcher.fire(Constants.EVENT_SET_PAGE, Constants.PAGE_PROJECT_EDIT);
    }

    render() {
        var projects = App.myProjects.map((project, i) => {
            return (
                <div
                    className="card" 
                    key={i}
                    onPointerUp={this.onProjectClicked.bind(this, project)}>
                    {project.name}
                </div>
            );
        });
        return (
            <div>
                <h2 className="content_title">
                    Befintliga projekt
                </h2>
                <div className="project_explore">
                    {projects}
                </div>
                <Button 
                    text="Nytt projekt"
                    onClick={this.onNewProjectClicked.bind(this, null)}/>
            </div>
        );
    }
	
}

export default Explore;