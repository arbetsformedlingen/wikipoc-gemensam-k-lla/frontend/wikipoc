import React from 'react';
import EventDispatcher from './../../context/event_dispatcher.jsx';
import Constants from './../../context/constants.jsx';
import App from './../../context/app.jsx';
import Button from './../../control/button.jsx';
import ModeWhiteboard from './mode_whiteboard.jsx';
import ModeGrid from './mode_grid.jsx';
import ModeGrid2 from './mode_grid2.jsx';
import ModeGrid3 from './mode_grid3.jsx';
import ModeGrid4 from './mode_grid4.jsx';

class View extends React.Component { 

	constructor() {
        super();
        this.state = {
            mode: 0,
            isSaving: false,
        };
        this.boundShowSave = this.showSave.bind(this);
        this.boundHideSave = this.hideSave.bind(this);
    }

    componentDidMount() {
        EventDispatcher.add(this.boundShowSave, Constants.EVENT_SAVE_SHOW);
        EventDispatcher.add(this.boundHideSave, Constants.EVENT_SAVE_HIDE);
    }

    componentWillUnmount() {
        EventDispatcher.remove(this.boundShowSave);
        EventDispatcher.remove(this.boundHideSave);
    }

    showSave() {
        this.setState({isSaving: true});
    }
    
    hideSave() {
        this.setState({isSaving: false});
    }
    
    position(card, x, y) {
        var currentY = y;
        card.connections.forEach((connection) => {
            connection.connections.forEach((child) => {
                var process = true;
                if(child.numIncomingConnections() > 1) {
                    if(card != child.inboundConnections[0].card) {
                        process = false;
                    }
                }
                if(process) {
                    currentY = this.position(child, x + card.width + 200, currentY);
                }
            });
        });
        //position self
        card.x = x;
        if(currentY - y < card.height) {
            card.y = y;
            currentY = y + card.height;
        } else {
            card.y = y + (currentY - y) * 0.5 - card.height * 0.5;
        }
        return currentY + 20;
    }

    rearrangeCards() {
        var cards = App.project.cards;
        //find all root cards
        var roots = cards.filter((c) => c.numIncomingConnections() == 0);
        var top = 0;
        roots.forEach((root) => {
            top = this.position(root, 0, top);
        });
    }

    renderWorkModeMenu() {
        return (
            <div className="project_view_work_mode">
                <div
                    onPointerUp={() => this.setState({mode: 0})}>
                    {Constants.ICON_WHITEBOARD}
                </div>
                <div
                    onPointerUp={() => this.setState({mode: 3})}
                    style={{background:"#99f"}}>
                    {Constants.ICON_GRID}
                </div>
                <div
                    onPointerUp={() => this.setState({mode: 4})}
                    style={{background:"#f9f"}}>
                    {Constants.ICON_GRID}
                </div>
            </div>
        );
    }

    renderHeader() {
        return (
            <div className="project_view_header">
                {App.project.name.length > 0 &&
                    <h1>{App.project.name}</h1>
                }
                <div className="project_view_menu">                    
                    <Button 
                        text="Spara"
                        onClick={() => App.saveToGitlab(App.project)}/>
                    {this.renderWorkModeMenu()}
                    {this.state.mode == 0 &&
                        <div className="project_view_menu_order">
                            <Button 
                                text="Ordna kort"
                                onClick={() => this.rearrangeCards()}/>
                        </div>
                    }
                </div>
            </div>
        );
    }

    render() {        
        return (
            <div className="project_view">
                {this.renderHeader()}
                {this.state.mode == 0 &&
                    <ModeWhiteboard/>
                }
                {this.state.mode == 1 &&
                    <ModeGrid/>
                }
                {this.state.mode == 2 &&
                    <ModeGrid2/>
                }
                {this.state.mode == 3 &&
                    <ModeGrid3/>
                }
                {this.state.mode == 4 &&
                    <ModeGrid4/>
                }
                {this.state.isSaving &&
                    <div className="save_container">
                        <div>Sparar</div>
                        <div className="save_indicator"/>
                    </div>
                }
            </div>
        );       
    }
	
}

export default View;