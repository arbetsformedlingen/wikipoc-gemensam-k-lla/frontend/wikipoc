import React from 'react';
import EventDispatcher from './../../context/event_dispatcher.jsx';
import Constants from './../../context/constants.jsx';
import App from './../../context/app.jsx';
import EditCard from './edit_card.jsx';

class ModeGrid3 extends React.Component { 

	constructor() {
        super();
        this.state = {
            editCard: null,
        };
        this.boundEditCard = this.onEditCardClicked.bind(this);
    }

    componentDidMount() {
        EventDispatcher.add(this.boundEditCard, Constants.EVENT_EDIT_CARD);
    }

    componentWillUnmount() {
        EventDispatcher.remove(this.boundEditCard);
    }

    onEditCardClicked(card) {
        this.setState({editCard: card});
    }

    renderGrid(cards) {
        var getTags = () => {
            var tags = [];
            if(cards) {
                for(var i=0; i<cards.length; ++i) {
                    var card = cards[i];                
                    for(var j=0; j<card.tags.length; ++j) {
                        if(tags.indexOf(card.tags[j]) == -1) {
                            tags.push(card.tags[j]);
                        }
                    }
                }
            }
            return tags;
        }
        var renderCard = (card, index) => {
            return (
                <div
                    className="mode_grid_column_card"
                    key={index}>
                    <div>{card.name}</div>
                    <div>{card.description}</div>
                    <div 
                        className="mode_grid_column_card_edit"
                        onPointerUp={() => this.onEditCardClicked(card)}>
                        {Constants.ICON_EDIT}
                    </div>
                    <div>#{card.id}</div>
                </div>
            );
        };
        var renderColumnCards = (tag, key) => {
            //nivå 2 - moduler / delområde
            //nivå 3 - 
            //nivå 4 - exempel
            var title = "Kort";
            var columnCards = [];
            if(cards) {
                if(tag == "") {
                    columnCards = cards.filter((card) => card.tags.length == 0 && card.numIncomingConnections() > 0 && card.numOutgoingConnections() == 0);
                } else if(tag == "__root__") {
                    columnCards = cards.filter((card) => card.numIncomingConnections() == 0);
                    title = "Delkvalifikation";
                } else if(tag == "__mid__") {
                    columnCards = cards.filter((card) => card.numIncomingConnections() > 0 && card.numOutgoingConnections() > 0);
                    title = "Moduler";
                } else {
                    columnCards = cards.filter((card) => card.tags.indexOf(tag) >= 0);
                    title = "#" + tag;
                }
            }
            if(columnCards.length > 0) {
                return (
                    <div                     
                        key={key}>
                            <div className="mode_grid_column_header">
                                {title}
                            </div>
                            {columnCards.map(renderCard)}
                    </div>
                );
            }
        }

        var tags = getTags();
        // make sure cards without tags gets included
        tags.unshift("");
        tags.unshift("__mid__");
        tags.unshift("__root__");
        
        return (
            <div className="mode_grid_group">
                {tags.map(renderColumnCards)}
            </div>
        );
    }

    render() {        
        return (
            <div className="project_view mode_grid_container">   
                <div className="mode_grid_groups">
                    {this.renderGrid(App.project.cards)}
                </div>
                {this.state.editCard != null &&
                    <EditCard 
                        card={this.state.editCard}
                        modeGrid={true}/>
                }
            </div>
        );       
    }
	
}

export default ModeGrid3;
