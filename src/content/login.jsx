import React from 'react';
import ReactDOM from 'react-dom';
import EventDispatcher from './../context/event_dispatcher.jsx';
import App from './../context/app.jsx';
import Constants from './../context/constants.jsx';
import Button from './../control/button.jsx';
import Link from './../control/link.jsx';
import InputField from './../control/input_field.jsx';

class Login extends React.Component { 

	constructor() {
        super();
        this.email = null;
        this.password = null;
    }

    onLoginClicked() {
        var raw = localStorage.getItem("wiki_user");
        if(raw == null) {
            return;
        }
        var user = JSON.parse(raw);
        if(user.email != this.email) {
            return;
        }
        if(user.password != this.password) {
            return;
        }
        // TODO: validate login before calling these..
        App.onUserLoggedIn(user);
        EventDispatcher.fire(Constants.EVENT_SET_PAGE, App.targetPage ? App.targetPage : Constants.PAGE_PROJECT);
        App.targetPage = null;
    }
    
    onForgotPaswordClicked() {
        
    }
    
    onRegisterClicked() {
        EventDispatcher.fire(Constants.EVENT_SET_PAGE, Constants.PAGE_REGISTER);
    }

    render() {        
        return (
            <div className="login">
                <div>
                    <InputField 
                        title="Epost"
                        onValueChange={(v) => this.email = v}/>
                    <InputField 
                        title="Lösenord"
                        isPassword={true}
                        onValueChange={(v) => this.password = v}/>
                    <Button 
                        text="Logga in"
                        onClick={this.onLoginClicked.bind(this)}/>
                    <Link 
                        css="login_center"
                        text="Har du glömt lösenordet?"
                        onClick={this.onForgotPaswordClicked.bind(this)}/>
                    <div className="login_divider"/>
                    <Button 
                        css="login_register"
                        text="Skapa nytt konto"
                        onClick={this.onRegisterClicked.bind(this)}/>
                </div>
            </div>
        );
    }
	
}

export default Login;