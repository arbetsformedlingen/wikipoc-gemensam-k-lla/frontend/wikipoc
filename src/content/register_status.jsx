import React from 'react';
import ReactDOM from 'react-dom';
import Button from './../control/button.jsx';
import App from './../context/app.jsx';
import Constants from './../context/constants.jsx';
import EventDispatcher from './../context/event_dispatcher.jsx';

class RegisterStatus extends React.Component { 

	constructor() {
        super();
    }
    
    onNextClicked() {
        EventDispatcher.fire(Constants.EVENT_SET_PAGE, App.targetPage ? App.targetPage : Constants.PAGE_PROJECT);
        App.targetPage = null;
    }

    render() {        
        return (
            <div className="register">
                <div className="register_staus">
                    <h1>
                        Du är nu registrerad!
                    </h1>
                    <Button 
                        text="Nästa"
                        onClick={this.onNextClicked.bind(this)}/>
                </div>
            </div>
        );
    }
	
}

export default RegisterStatus;