import React from 'react';
import ReactDOM from 'react-dom';
import App from './../context/app.jsx';
import Constants from './../context/constants.jsx';
import EventDispatcher from './../context/event_dispatcher.jsx';

class Menu extends React.Component { 

	constructor() {
        super();
        this.state = {
            page: App.page,
        };
        this.boundPageChanged = this.onPageChanged.bind(this);
    }

    componentDidMount() {
        EventDispatcher.add(this.boundPageChanged, Constants.EVENT_SET_PAGE);
    }

    componentWillUnmount() {
        EventDispatcher.remove(this.boundPageChanged);
    }

    onPageChanged(page) {
        var index = [
            Constants.PAGE_PROJECT_EXPLORE, Constants.PAGE_PROJECT, 
            Constants.PAGE_MATERIAL_EXPLORE, Constants.PAGE_MATERIAL
        ].indexOf(page);
        if(index != -1) {
            App.project = null;
        }
        this.setState({page: page});
    }
    
    onProjectClicked() {
        EventDispatcher.fire(Constants.EVENT_SET_PAGE, Constants.PAGE_PROJECT);
    }
    
    onExploreClicked() {
        EventDispatcher.fire(Constants.EVENT_SET_PAGE, Constants.PAGE_EXPLORE);
    }
    
    onMaterialClicked() {
        EventDispatcher.fire(Constants.EVENT_SET_PAGE, Constants.PAGE_MATERIAL);
    }

    renderOption(id, text, onClick) {
        return (
            <div 
                className={this.state.page.indexOf(id) >= 0 ? "selected" : ""}
                onPointerUp={onClick}>
                {text}
            </div>
        );
    }

    render() {        
        return (
            <div className="menu">
                {this.renderOption(Constants.PAGE_PROJECT, "Projekt", this.onProjectClicked.bind(this))}
                {this.renderOption(Constants.PAGE_EXPLORE, "Utforska", this.onExploreClicked.bind(this))}
                {this.renderOption(Constants.PAGE_MATERIAL, "Material", this.onMaterialClicked.bind(this))}
            </div>
        );
    }
	
}

export default Menu;