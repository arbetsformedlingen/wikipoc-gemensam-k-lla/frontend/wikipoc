
class Util {

    vec2(x, y) {
        return {
            x: x,
            y: y,
        };
    }

    vec2Sub(a, b) {
        return {
            x: a.x - b.x,
            y: a.y - b.y,
        };
    }
    
    vec2Add(a, b) {
        return {
            x: a.x + b.x,
            y: a.y + b.y,
        };
    }
    
    vec2Mul(a, b) {
        return {
            x: a.x * b.x,
            y: a.y * b.y,
        };
    }
    
    vec2MulReal(v, real) {
        return {
            x: v.x * real,
            y: v.y * real,
        };
    }
    
    vec2DivReal(v, real) {
        return {
            x: v.x / real,
            y: v.y / real,
        };
    }

    vec2Length(v) {
        return Math.sqrt(v.x * v.x + v.y * v.y);
    }

    vec2Normalize(v) {
        var l = this.vec2Length(v);
        return {
            x: v.x / l,
            y: v.y / l,
        };
    }

    vec2Lerp(a, b, t) {
        return {
            x: a.x + (b.x - a.x) * t,
            y: a.y + (b.y - a.y) * t
        };
    }

    closestPointToSegment(a, b, p) {
        var ap = this.vec2Sub(p, a);
        var ab = this.vec2Sub(b, a);
        var magnitude = ab.x * ab.x + ab.y * ab.y;
        var abapDot = ap.x * ab.x + ap.y * ab.y;
        var distance = abapDot / magnitude;
        if( distance < 0.0 ) {
            return a;
        } else if( distance > 1.0 ) {
            return b;
        }
        return this.vec2Add(a, this.vec2MulReal(ab, distance));
    }

    closestPointToBezierCurve(curve, p) {
        // NOTE: im sure there is a much better way then this
        var bestDistance = 9999999.9;
        var bestPoint = null;
        for(var i=0; i<curve.length - 1; ++i) {
            var point = this.closestPointToSegment(curve[i], curve[i + 1], p);
            var d = this.vec2Length(this.vec2Sub(point, p));
            if(d < bestDistance) {
                bestDistance = d;
                bestPoint = point;
            }
        }
        return bestPoint;
    }

    hitTestBezier(curve, p, maxDistance) {        
        return this.distanceToBezier(curve, p) < maxDistance;
    }

    distanceToBezier(curve, p) {
        var c = this.closestPointToBezierCurve(curve, p);
        return this.vec2Length(this.vec2Sub(c, p));
    }

    bezierPoint(location, control) {
        // control points are in localspace relative to location
        return {
            location: location,
            control: control
        };
    }

    bezierCurve(from, to, segments) {
        // control points in world
        var cp1 = this.vec2Add(from.location, from.control);
        var cp2 = this.vec2Add(to.location, to.control);
        // calculate curve points
        var bezier = [from.location];
        for(var j=1; j<segments; ++j) {
            var prc = j / segments;
            // interpolate between source points and control points
            var a1 = this.vec2Lerp(from.location, cp1, prc);
            var b1 = this.vec2Lerp(cp2, to.location, prc);
            // interpolate between control points
            var c = this.vec2Lerp(cp1, cp2, prc);
            // interpolate between a1 -> c and c -> b1
            var a = this.vec2Lerp(a1, c, prc);
            var b = this.vec2Lerp(c, b1, prc);
            // finalize curve point
            var v = this.vec2Lerp(a, b, prc);
            bezier.push(v);
        }
        bezier.push(to.location);
        return bezier;
    }

}

export default new Util();