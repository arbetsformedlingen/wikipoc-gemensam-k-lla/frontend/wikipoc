import React from 'react';

class EventDispatcher { 

    constructor() {
        this.listeners = [];
    }

    add(callback, event) {
        var o = this.listeners.find((e) => {
            return e.callback == callback && e.event == event;
        });
        if(!o) {
            this.listeners.push({
                event: event,
                callback: callback
            });
        }
    }

    remove(callback) {
        var listeners = this.listeners.filter((x) => {
            return x.callback == callback;
        });
        for(var i=0; i<listeners.length; ++i) {
            this.listeners.splice(this.listeners.indexOf(listeners[i]), 1);
        }
    }

    fire(event, data) {        
        for(var i=0; i<this.listeners.length; ++i) {
            var o = this.listeners[i];
            if(o.event == event) {
                o.callback(data);
            }
        }
    }

    bindEvent(event, data) {
        return this.fire.bind(this, event, data);
    }
	
}

export default new EventDispatcher;