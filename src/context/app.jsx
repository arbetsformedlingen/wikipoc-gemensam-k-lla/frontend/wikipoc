import Constants from './constants.jsx';
import EventDispatcher from './event_dispatcher.jsx';
import Rest from './rest.jsx';
import Card from './../component/card.jsx';

class App {

    constructor() {
        this.user = null;
        this.page = Constants.PAGE_START;
        this.targetPage = null;
        this.files = [];
        this.myFiles = [];
        this.projects = [];
        this.myProjects = [];
        this.project = null;
        this.file = null;
        this.tags = ["tag 1", "tag 2", "tag 3"];
        this.connectionTypes = [
            "Broader",
            "Narrower",
            "Related",
        ];
    }

    onInit() {
        // preload fonts used by whiteboard renderer
        document.fonts.load("18px Nunito Sans");
        document.fonts.load("16px Nunito Sans");
        document.fonts.load("14px Nunito Sans");
        document.fonts.load("12px Nunito Sans");
        document.fonts.load("10px Nunito Sans");
        try {
            var raw = localStorage.getItem("wiki_user");
            if(raw) {
                var user = JSON.parse(raw);
                var token = JSON.parse(localStorage.getItem("wiki_token"));
                // NOTE: this is just for testing, verification of token will be managed by backend
                if((Date.now() - token) < 1000 * 60 * 60 * 4) {
                    this.onUserLoggedIn(user);
                }
            }
        } catch(e) {
        }
        this.fetchFromGitlab();
    }

    onUserLoggedIn(user) {
        this.user = user;
        // TODO: get a real token from backend, let backend validate token expiration time
        localStorage.setItem("wiki_token", Date.now());
    }

    createProject() {
        return {
            cards: [],
            name: "",
            nextId: 1,
            id: Date.now(),
        };
    }

    pack(project) {
        if(project.id == null) {
            project.id = Date.now();
        }
        var obj = {
            id: project.id,
            nextId: project.nextId,
            name: project.name,
            description: project.description,
            cards: [],
            connections: [],
        };

        for(var i=0; i<project.cards.length; ++i) {
            var card = project.cards[i];
            var connections = card.connections.map((connection) => {
                return {
                    name: connection.name,
                    ids: connection.connections.map((card) => card.id)
                };
            });

            obj.cards.push({
                name: card.name,
                description: card.description,
                id: card.id,
                x: card.x,
                y: card.y,
                connections: connections,
                tags: card.tags,
            });
        }

        return obj;
    }

    unpack(obj) {
        var cards = obj.cards.map((data) => {
            var card = new Card();
            card.x = data.x;
            card.y = data.y;
            card.name = data.name;
            card.description = data.description;
            card.id = data.id;
            card.tags = data.tags;
            return card;
        });

        for(var i=0; i<cards.length; ++i) {
            var connections = obj.cards[i].connections;
            var card = cards[i];
            for(var j=0; j<connections.length; ++j) {
                var node = card.addConnectionNode(connections[j].name);
                connections[j].ids.map((id) => {
                    var card = cards.find((x) => x.id == id);
                    node.connect(card);
                });                
            }
        }

        var project = {
            id: obj.id,
            nextId: obj.nextId,
            name: obj.name,
            description: obj.description,
            cards: cards,
        };

        return project;
    }

    fetchFromGitlab() {
        Rest.getFilesFromGitLab((data)=>{
            console.log(data);
            var o = JSON.parse(data);
            for(var i=0; i<o.length; ++i) {
                if(o[i].name.indexOf(".json") != -1) {
                    Rest.getFileFromGitLab(o[i].name, (d)=>{
                        this.myProjects.push(this.unpack(JSON.parse(d)));
                    },(s)=> {
                        console.log(s);        
                    });
                }
            }
        }, (status)=> {
            console.log(status);
        });
    }

    saveToGitlab(project) {
        var query = {
                "branch": "main",
                "commit_message": "some commit message",
                "actions": [{
                    "action": "create",
                    "file_path": project.id + ".json",
                    "content": JSON.stringify(this.pack(project))
                }]
            };
        EventDispatcher.fire(Constants.EVENT_SAVE_SHOW);
        Rest.postBody(JSON.stringify(query),(data) => {
            console.log(data);
            EventDispatcher.fire(Constants.EVENT_SAVE_HIDE);
        }, (status) => {
            console.log(status);
            if(status == 400) {
                query.actions[0].action = "update";
                Rest.postBody(JSON.stringify(query),(data) => {
                    console.log("Success2", data);
                }, (status) => {
                    console.log("Error2", status);
                });
            }
            EventDispatcher.fire(Constants.EVENT_SAVE_HIDE);
        });
    }

}

export default new App();