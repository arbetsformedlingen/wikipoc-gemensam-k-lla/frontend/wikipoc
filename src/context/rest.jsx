import React from 'react';
import Constants from './constants.jsx';

class Rest {

    setupCallbacks(http, onSuccess, onError) {
        this.currentRequest = http;
        http.onerror = () => {
            if(onError != null) {
                onError(http.status);
            }
        }
        http.onload = () => {
            if(http.status >= 200 && http.status < 300) {
                if(onSuccess != null) {
                    onSuccess(http.response);
                }
            } else {
                if(onError != null) {
                    onError(http.status);
                }
            }
        }
    }

    abort() {
        if(this.currentRequest) {
            this.currentRequest.abort();
        }
        if(this.currentErrorCallback) {
            this.currentErrorCallback(499); //Client Closed Request
        }
        this.currentRequest = null;
    }

    postBody(body, onSuccess, onError) {
        var http = new XMLHttpRequest();
        this.setupCallbacks(http, onSuccess, onError);
        http.open("POST", "https://wikipoc-nginx-secure-wikipoc-data.apps.testing.services.jtech.se/gitlab/repository/commits", true);
        http.setRequestHeader("Authorization", "Basic " + btoa("user1:SolOchBad2023"));
        http.setRequestHeader("Accept", "application/json");
        http.setRequestHeader("Content-Type", "application/json");
        http.send(body);
    }

    //http://wikipoc-nginx-wikipoc-data.apps.testing.services.jtech.se/gitlab/repository/commits
    /*{
        "branch": "main",
        "commit_message": "some commit message",
        "actions": [
          {
            "action": "create",
            "file_path": "myfile2.txt",
            "content": "some content"
          }
        ]
      }*/

    getFilesFromGitLab(onSuccess, onError) {
        //this.abort();
		var http = new XMLHttpRequest();
        this.setupCallbacks(http, onSuccess, onError);
        http.open("GET", "https://wikipoc-nginx-secure-wikipoc-data.apps.testing.services.jtech.se/gitlab/repository/tree", true);
        http.setRequestHeader("Authorization", "Basic " + btoa("user1:SolOchBad2023"));//"dXNlcjE6U29sT2NoQmFkMjAyMw=="
        http.setRequestHeader("Accept", "application/json");
        http.send();
    }

    getFileFromGitLab(name, onSuccess, onError) {
        //this.abort();
		var http = new XMLHttpRequest();
        this.setupCallbacks(http, onSuccess, onError);
        http.open("GET", "https://wikipoc-nginx-secure-wikipoc-data.apps.testing.services.jtech.se/gitlab/repository/files/" + name + "/raw", true);
        http.setRequestHeader("Authorization", "Basic " + btoa("user1:SolOchBad2023"));
        http.setRequestHeader("Accept", "application/json");
        http.send();
    }

}

export default new Rest;
