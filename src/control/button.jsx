import React from 'react';
import ReactDOM from 'react-dom';

class Button extends React.Component { 

	constructor() {
        super();
    }

    render() {        
        return (
            <div 
                className={this.props.css ? "button " + this.props.css : "button"}
                onPointerUp={this.props.onClick}>
                {this.props.text}
            </div>
        );
    }
	
}

export default Button;