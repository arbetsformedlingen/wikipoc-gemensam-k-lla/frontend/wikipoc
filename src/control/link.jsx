import React from 'react';
import ReactDOM from 'react-dom';

class Link extends React.Component { 

	constructor() {
        super();
    }

    render() {        
        return (
            <div 
                className={this.props.css ? "link " + this.props.css : "link"}
                onPointerUp={this.props.onClick}>
                {this.props.text}
            </div>
        );
    }
	
}

export default Link;