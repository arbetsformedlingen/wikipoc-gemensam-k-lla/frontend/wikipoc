import React from 'react';
import ReactDOM from 'react-dom';

class InputField extends React.Component { 

	constructor(props) {
        super(props);
        this.state = {
            value: props.value ? props.value : "",
        }
    }

    onValueChanged(e) {
        if(this.props.onValueChange != null) {
            this.props.onValueChange(e.target.value);
        }
        this.setState({value: e.target.value});
    }

    render() {        
        return (
            <div className={this.props.css ? "login_field " + this.props.css : "login_field"}>
                <div>{this.props.title}</div>
                <input 
                    className="rounded"
                    type={this.props.isPassword ? "password" : "text"}
                    value={this.state.value}
                    onChange={this.onValueChanged.bind(this)}/>
            </div>
        );
    }
	
}

export default InputField;