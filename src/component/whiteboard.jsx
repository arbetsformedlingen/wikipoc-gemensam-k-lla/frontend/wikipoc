
import { Renderable, RenderableConstants } from './renderable.jsx';
import Card from './card.jsx';
import Util from './../context/util.jsx';
import Constants from './../context/constants.jsx';
import EventDispatcher from './../context/event_dispatcher.jsx';

class Whiteboard {

    constructor() {
        this.width = 0;
        this.height = 0;
        this.parent = null;
        this.canvas = null;
        this.context = null; // render context
        this.renderables = [];
        this.hovered = null;
        this.hoveredConnection = null;
        this.dragged = null;
        this.target = null;
        this.draggedConnection = null;
        this.viewport = Util.vec2(0.0, 0.0);
        this.previousMouse = Util.vec2(0.0, 0.0);
        this.zoom = 0.0; // max zoomed in
        // states
        this.isRightMouseDown = false;
        this.isLeftMouseDown = false;
        this.isMiddleMouseDown = false;

        this.debug = false;

        // function binding
        this.boundResize = this.onResize.bind(this);
        this.boundMouseEnter = this.onMouseEnter.bind(this);
        this.boundMouseLeave = this.onMouseLeave.bind(this);
        this.boundMouseMove = this.onMouseMove.bind(this);
        this.boundMouseDown = this.onMouseDown.bind(this);
        this.boundMouseUp = this.onMouseUp.bind(this);
        this.boundMouseWheel = this.onMouseWheel.bind(this);
        this.boundRender = this.onRender.bind(this);
        this.boundDeleteCard = this.onDeleteCard.bind(this);
    }

    // takes a point in screen-space (0,0 -> w,h) and converts it to the worldspace
    toWorldSpace(screenSpacePoint) {
        var zoom = 1.0 - this.zoom;
        var xp = screenSpacePoint.x / this.width;
        var yp = screenSpacePoint.y / this.height;
        var x = this.width * (1.0 / zoom) * xp;
        var y = this.height * (1.0 / zoom) * yp;
        return Util.vec2(
            x - this.viewport.x, 
            y - this.viewport.y
        );
    }

    // takes a point in world-space and converts it to screen-space (0,0 -> w,h)
    toScreenSpace(worldPosition) {
        var p1 = this.toWorldSpace(Util.vec2(0.0, 0.0));
        var p2 = this.toWorldSpace(Util.vec2(this.width, this.height));
        var dimension = Util.vec2Sub(p2, p1);
        var x = worldPosition.x - p1.x;
        var y = worldPosition.y - p1.y;
        var xp = x / dimension.x;
        var yp = y / dimension.y;
        return Util.vec2(this.width * xp, this.height * yp);
    }

    sortRenderables() {
        this.renderables.sort((a, b) => {
            if(a.layer == b.layer) {
                return 0;
            }
            return a.layer < b.layer ? -1 : 1;
        });        
    }

    hitTestRenderables(point) {
        for(var i=this.renderables.length - 1; i>=0; --i) {
            var renderable = this.renderables[i];
            if(renderable.hitTest) {
                if(renderable.hitTest(point)) {
                    return renderable;
                }
            } else if(renderable.hitTestBounds(point)) {
                return renderable;
            }
        }
        return null;
    }

    hitTestConnection(point, hitDistance) {
        var source = null;
        var index = null
        var distance = null;
        this.foreachConnection((node, connection, i, location) => {
            var target = connection;
            var p1 = Util.bezierPoint(location, Util.vec2(50.0, 0.0));
            var p2 = Util.bezierPoint(Util.vec2(target.x, target.y + target.height * 0.5), Util.vec2(-50.0, 0.0));
            var curve = Util.bezierCurve(p1, p2, 50);
            // distance to curve
            var dist = Util.distanceToBezier(curve, point);
            if((distance == null || dist < distance) && dist < hitDistance) {
                distance = dist;
                source = node;
                index = i;
            }
        });
        if(source) {
            return {
                source: source,
                index: index,
                distance: distance
            };
        }
        return null;
    }

    foreachConnection(callback) {
        for(var i=0; i<this.renderables.length; ++i) {
            var processConnections = (node, socketLocation) => {
                for(var k=0; k<node.connections.length; ++k) {
                    callback(node, node.connections[k], k, socketLocation);
                }
            };
            var processCard = (renderable) => {
                for(var j=0; j<renderable.connections.length; ++j) {
                    processConnections(renderable.connections[j], renderable.getNodeOutputLocation(renderable.connections[j]));
                }
            };
            if(this.renderables[i].type == RenderableConstants.TYPE_CARD) {
                processCard(this.renderables[i]);
            }
        }
    }

    add(renderable) {
        renderable.context = this.context;
        renderable.canvas = this.canvas;
        renderable.whiteboard = this;
        if(renderable.onAttached) {
            renderable.onAttached();
        }
        this.renderables.push(renderable);
    }

    remove(renderable) {
        var index = this.renderables.indexOf(renderable);
        if(index != -1) {
            if(renderable.onDetached) {
                renderable.onDetached();
            }
            this.renderables.splice(index, 1);
        }
    }

    setEnableEvents(enabled) {
        if(enabled) {
            this.parent.addEventListener("mouseenter", this.boundMouseEnter);
            this.parent.addEventListener("mouseleave", this.boundMouseLeave);
            this.parent.addEventListener("mousemove", this.boundMouseMove);
            this.parent.addEventListener("mousedown", this.boundMouseDown);
            this.parent.addEventListener("mouseup", this.boundMouseUp);
            this.parent.addEventListener("wheel", this.boundMouseWheel);
        } else {
            this.parent.removeEventListener("mouseenter", this.boundMouseEnter);
            this.parent.removeEventListener("mouseleave", this.boundMouseLeave);
            this.parent.removeEventListener("mousemove", this.boundMouseMove);
            this.parent.removeEventListener("mousedown", this.boundMouseDown);
            this.parent.removeEventListener("mouseup", this.boundMouseUp);
            this.parent.removeEventListener("wheel", this.boundMouseWheel);
        }
    }

    onInit(canvas, width, height) {
        this.width = width;
        this.height = height;
        this.canvas = canvas;
        this.canvas.width = this.width;
        this.canvas.height = this.height;
        this.context = this.canvas.getContext("2d");
        // setup grid pattern
        var c = document.createElement("canvas");
        var size = 200;
        c.width = size;
        c.height = size;
        var ctx = c.getContext("2d");
        ctx.strokeStyle = "#e9e9e9";
        ctx.beginPath();
        ctx.moveTo(0, size * 0.5);
        ctx.lineTo(size, size * 0.5);
        ctx.moveTo(size * 0.5, 0);
        ctx.lineTo(size * 0.5, size);
        ctx.stroke();
        this.pattern = this.context.createPattern(c, "repeat");
        // setup callbacks
        EventDispatcher.add(this.boundDeleteCard, Constants.EVENT_DELETE_CARD);
        // setup render loop (~60 fps)
        this.renderId = window.requestAnimationFrame(this.boundRender);        
    }

    onClose() {
        this.context = null;
        window.cancelAnimationFrame(this.renderId);
        // remove callbacks
        EventDispatcher.remove(this.boundDeleteCard);
    }

    onBindEvents(element) {
        this.parent = element;
        // global events
        window.addEventListener("resize", this.boundResize);
        // input events
        element.addEventListener("mouseenter", this.boundMouseEnter);
        element.addEventListener("mouseleave", this.boundMouseLeave);
        element.addEventListener("mousemove", this.boundMouseMove);
        element.addEventListener("mousedown", this.boundMouseDown);
        element.addEventListener("mouseup", this.boundMouseUp);
        element.addEventListener("wheel", this.boundMouseWheel);
    }

    onUnbindEvents() {
        // global events
        window.removeEventListener("resize", this.boundResize);
        // input events
        this.parent.removeEventListener("mouseenter", this.boundMouseEnter);
        this.parent.removeEventListener("mouseleave", this.boundMouseLeave);
        this.parent.removeEventListener("mousemove", this.boundMouseMove);
        this.parent.removeEventListener("mousedown", this.boundMouseDown);
        this.parent.removeEventListener("mouseup", this.boundMouseUp);
        this.parent.removeEventListener("wheel", this.boundMouseWheel);
        this.parent = null;
    }

    onDeleteCard(card) {        
        this.remove(card);
    }

    onResize(e) {       
        this.width = this.parent.clientWidth;
        this.height = this.parent.clientHeight;
        this.canvas.width = this.width;
        this.canvas.height = this.height;
    }

    onMouseEnter(e) {
        this.previousMouse = Util.vec2(e.offsetX, e.offsetY);
    }
    
    onMouseLeave(e) {
        // clear states
        if(this.dragged) {
            this.dragged.visualState = RenderableConstants.STATE_NORMAL;
            this.dragged.onVisualStateChanged();
            this.dragged.layer = 0;
        }
        if(this.hovered) {
            this.hovered.visualState = RenderableConstants.STATE_NORMAL;
            this.hovered.onVisualStateChanged();
        }
        // reset states
        this.target = null;
        this.dragged = null;
        this.hovered = null;
        this.hoveredConnection = null;
        this.isRightMouseDown = false;
        this.isMiddleMouseDown = false;
        this.isLeftMouseDown = false;
    }

    onMouseMove(e) {
        if(e.target != this.canvas) {
            return;
        }
        this.canvas.style.cursor = "default";
        var screenMouse = Util.vec2(e.offsetX, e.offsetY);
        var mouse = this.toWorldSpace(screenMouse);
        var delta = Util.vec2Sub(screenMouse, this.previousMouse);
        if(this.dragged) {
            // drag renderable
            this.dragged.x = mouse.x + this.dragOffset.x;
            this.dragged.y = mouse.y + this.dragOffset.y;
        } else if(this.draggedConnection == null && ((this.isRightMouseDown && this.target == null) || this.isMiddleMouseDown)) {
            this.canvas.style.cursor = "grabbing";
            // move viewport
            this.viewport.x += delta.x * 2.0;
            this.viewport.y += delta.y * 2.0;
        } else {
            // hover over renderable
            var renderable = this.hitTestRenderables(mouse);
            if(this.hovered && this.hovered != renderable) {
                this.hovered.visualState = RenderableConstants.STATE_NORMAL;
                this.hovered.onVisualStateChanged();
            }
            if(renderable) {
                renderable.visualState = RenderableConstants.STATE_HOVERED;
                renderable.onVisualStateChanged();
                renderable.onMouseMove(Util.vec2Sub(mouse, Util.vec2(renderable.x, renderable.y)));
                if(this.target == renderable) {
                    // the user is moving the mouse while holding the mouse button down
                    // over a renderable, initiate dragging
                    this.dragged = this.target;
                    this.dragged.visualState = RenderableConstants.STATE_DRAGGED;
                    this.dragged.onVisualStateChanged();
                    this.dragged.layer = 1000000;
                    this.target = null;
                } else {
                    // we have moved the mouse out side of the target, thus we cant drag it
                    this.target = null;
                }
            }
            this.hovered = renderable;
            if(this.hovered == null) {
                // hover over connection
                this.hoveredConnection = this.hitTestConnection(mouse, 5.0);
                if(this.hoveredConnection != null) {
                    this.canvas.style.cursor = "pointer";
                }
            } else {
                this.hoveredConnection = null;
            }
        }
        this.previousMouse = screenMouse;
    }

    onMouseDown(e) {
        if(e.target != this.canvas) {
            return;
        }
        if(e.button == 0) {
            this.isRightMouseDown = true;
        } else if(e.button == 1) {
            this.isMiddleMouseDown = true;
        } else if(e.button == 2) {
            this.isLeftMouseDown = true;
        }
        var p = this.toWorldSpace(Util.vec2(e.offsetX, e.offsetY));
        // if the user clicks on a renderable, mark it as a target for actions
        this.target = this.hitTestRenderables(p);
        if(this.target) {
            if(this.target.type == RenderableConstants.TYPE_CARD) {
                this.draggedConnection = this.target.hitTestConnectionNode(p);
            }
            if(this.draggedConnection) {
                this.draggedConectionStart = this.target.getNodeOutputLocation(this.draggedConnection);
                this.target = null;
            }
            if(this.target) {
                // save offset so dragging is smooth and not snapping
                this.dragOffset = Util.vec2Sub(Util.vec2(this.target.x, this.target.y), p);
            }
        }
    }

    onMouseUp(e) {
        if(e.target != this.canvas) {
            return;
        }
        var p = this.toWorldSpace(Util.vec2(e.offsetX, e.offsetY));
        var renderable = this.hitTestRenderables(p);
        if(this.hoveredConnection != null) {
            this.hoveredConnection.source.disconnect(this.hoveredConnection.source.connections[this.hoveredConnection.index]);
        }
        if(this.draggedConnection && renderable && renderable != this.draggedConnection.card) {
            // try to create a connection between cards
            if(renderable.type == RenderableConstants.TYPE_CARD) {
                this.draggedConnection.connect(renderable);
            }
        }
        if(renderable != null && renderable == this.target && this.isRightMouseDown && e.button == 0) {
            // click event for renderable
            if(renderable.onClick) {
                // convert mouse to local space
                p.x -= renderable.x;
                p.y -= renderable.y;
                renderable.onClick(p);
            }
        }        
        if(e.button == 0) {
            this.isRightMouseDown = false;
        } else if(e.button == 1) {
            this.isMiddleMouseDown = false;
        } else if(e.button == 2) {
            this.isLeftMouseDown = false;
        }
        // clear any drag target
        if(this.dragged) {
            if(renderable == this.dragged) {
                this.dragged.visualState = RenderableConstants.STATE_HOVERED;
            } else {
                this.dragged.visualState = RenderableConstants.STATE_NORMAL;
            }
            this.dragged.onVisualStateChanged();
            this.dragged.layer = 0;
        }
        this.target = null;
        this.dragged = null;
        this.draggedConnection = null;
    }

    onMouseWheel(e) {
        if(e.target != this.canvas) {
            return;
        }
        var prev = this.zoom;
        this.zoom -= (e.wheelDelta / 120) * 0.05;
        this.zoom = Math.max(0, Math.min(this.zoom, 0.75));
        // adjust viewport so the zoom is centered
        var nw = this.width * (1 / (1.0 - this.zoom));
        var nh = this.height * (1 / (1.0 - this.zoom));
        var pw = this.width * (1 / (1.0 - prev));
        var ph = this.height * (1 / (1.0 - prev));
        this.viewport.x += (nw - pw) * 0.5;
        this.viewport.y += (nh - ph) * 0.5;
    }

    onRender() {
        if(this.context == null) {
            return;
        }

        var renderCurve = (curve) => {
            this.context.beginPath();
            this.context.moveTo(curve[0].x, curve[0].y);
            for(var i=1; i<curve.length; ++i) {
                this.context.lineTo(curve[i].x, curve[i].y);
            }
            this.context.stroke();
        };

        // clear previous frame
        this.context.setTransform(1, 0, 0, 1, 0, 0); // default transform so we clear the correct area
        this.context.clearRect(0, 0, this.width, this.height);

        // apply viewport
        var zoom = 1.0 - this.zoom;
        this.context.scale(zoom, zoom);
        this.context.translate(this.viewport.x, this.viewport.y);
        
        // grid
        this.context.fillStyle = this.pattern;
        var p1 = this.toWorldSpace(Util.vec2(0.0, 0.0));
        var p2 = this.toWorldSpace(Util.vec2(this.width, this.height));
        this.context.fillRect(p1.x, p1.y, p2.x - p1.x, p2.y - p1.y);
        
        // prepare frame
        this.context.fillStyle = "#000";
        this.context.strokeStyle = "#ccc";
        this.context.lineWidth = 1;

        this.sortRenderables();

        // render connections
        this.foreachConnection((node, connection, index, location) => {
            var target = connection;
            var p1 = Util.bezierPoint(location, Util.vec2(50.0, 0.0));
            var p2 = Util.bezierPoint(Util.vec2(target.x, target.y + target.height * 0.5), Util.vec2(-50.0, 0.0));
            var curve = Util.bezierCurve(p1, p2, 50);
            // render curve
            this.context.strokeStyle = "#000";
            this.context.lineWidth = 2.0;
            if(node.state == RenderableConstants.STATE_HOVERED) {
                this.context.strokeStyle = "#fcba03";
                this.context.lineWidth = 4.0;
                target.borderColor = "#fcba03";
                target.borderThickness = 4.0;
            } else {
                target.borderColor = "#bbb";
                target.borderThickness = 2.0;
            }
            if(this.hoveredConnection != null && this.hoveredConnection.source == node && this.hoveredConnection.index == index) {
                this.context.strokeStyle = "#fcba03";
                this.context.lineWidth = 4.0;
            }
            renderCurve(curve);
        });

        // render objects
        for(var i=0; i<this.renderables.length; ++i) {
            var renderable = this.renderables[i];
            this.context.save();
            this.context.translate(renderable.x, renderable.y);
            renderable.onRender(this.context);
            this.context.restore();
        }
        
        // user is dragging a new connection
        if(this.draggedConnection) {
            var p1 = Util.bezierPoint(this.draggedConectionStart, Util.vec2(50.0, 0.0));
            var p2 = Util.bezierPoint(this.toWorldSpace(this.previousMouse), Util.vec2(0.0, 0.0));
            var curve = Util.bezierCurve(p1, p2, 50);
            // render curve
            this.context.strokeStyle = "#000";
            this.context.lineWidth = 2.0;
            renderCurve(curve);
        }

        // active card, user is configuring properties
        if(this.activeCard) {
            this.context.fillStyle = "#fffb";
            this.context.fillRect(p1.x, p1.y, p2.x - p1.x, p2.y - p1.y);
            this.context.save();
            this.context.translate(this.activeCard.x, this.activeCard.y);
            this.activeCard.onRender(this.context);
            this.context.restore();
        }

        if(this.debug) {
            // prev mouse
            var p = this.toWorldSpace(this.previousMouse);
            this.context.strokeStyle = "#f00";
            this.context.beginPath();
            this.context.moveTo(p.x - 10, p.y);
            this.context.lineTo(p.x + 10, p.y);
            this.context.stroke();
            this.context.beginPath();
            this.context.moveTo(p.x, p.y - 10);
            this.context.lineTo(p.x, p.y + 10);
            this.context.stroke();
        }

        // request rendering of next frame
        this.renderInterval = window.requestAnimationFrame(this.boundRender);
    }

}

export default Whiteboard;