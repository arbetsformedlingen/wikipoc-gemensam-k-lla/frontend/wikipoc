
const RenderableConstants = {
    // states
    STATE_NORMAL: 0,
    STATE_HOVERED: 1,
    STATE_PRESSED: 2,
    STATE_DRAGGED: 3,
    // types
    TYPE_UNKOWN: 0,
    TYPE_CARD: 1,
};

class Renderable {

    constructor() {
        // properties
        this.x = 0;
        this.y = 0;
        this.layer = 0;
        this.visualState = RenderableConstants.STATE_NORMAL;
        this.type = RenderableConstants.TYPE_UNKOWN;
        this.context = null; // canvas context
        this.canvas = null; 
        this.whiteboard = null;
        // bounding box
        this.width = 0;
        this.height = 0;
    }

    hitTestBounds(point) {
        if(point.x < this.x) return false;
        if(point.y < this.y) return false;
        if(point.x > this.x + this.width) return false;
        if(point.y > this.y + this.height) return false;
        return true;
    }

}

export { Renderable, RenderableConstants };