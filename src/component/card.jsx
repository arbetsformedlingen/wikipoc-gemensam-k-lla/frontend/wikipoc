
import Util from './../context/util.jsx';
import Constants from './../context/constants.jsx';
import EventDispatcher from './../context/event_dispatcher.jsx';
import Layout from './layout.jsx';
import { Renderable, RenderableConstants } from './renderable.jsx';

class Card extends Renderable {

    constructor() {
        super();
        this.MAXWIDTH = 450.0;
        this.MINWIDTH = 300.0;
        this.type = RenderableConstants.TYPE_CARD;
        this.width = this.MINWIDTH;
        this.height = 90;
        this.name = "";
        this.description = null;
        this.tags = [];
        this.connections = [];
        this.hoveredConnection = null;
        this.inboundConnections = [];
        this.expanded = false;
        this.borderThickness = 2.0;
        this.borderColor = "#bbb";
        this.id = 0;
        // sub component layouts
        this.nameLayout = new Layout();
        this.nameLayout.padding.left = 20;
        this.nameLayout.padding.right = 20;
        this.nameLayout.onCalculate = (layout) => {
            this.setFont(16);
            var w = this.context.measureText(this.name).width;
            return Util.vec2(w, this.name.length > 0 ? 90.0 : 0.0);
        };
        this.nameLayout.onTransform = (layout, context) => {
            context.translate(layout.padding.left, layout.actualHeight * 0.5);
        };
        this.nameLayout.onRender = (layout, context) => {
            // name
            context.fillStyle = "#030DFE";
            context.font = "bold 16px Nunito Sans";
            context.textBaseline = "middle";
            context.fillText(this.name, 0.0, 0.0);
            // underline
            context.strokeStyle = "#030DFE";
            context.lineWidth = 0.75;
            context.beginPath();
            context.moveTo(0.0, 6);
            context.lineTo(layout.width, 6);
            context.stroke();
        };
        this.connectionsLayout = new Layout();
        this.connectionsLayout.padding.left = 0;
        this.connectionsLayout.padding.right = -10;
        this.connectionsLayout.padding.top = 40;
        this.connectionsLayout.padding.bottom = 20;
        this.connectionsLayout.onCalculate = (layout) => {
            var maxWidth = 0;
            this.setFont(12);
            for(var i=0; i<this.connections.length; ++i) {
                var w = this.context.measureText(this.connections[i].name).width;
                if(w > maxWidth) {
                    maxWidth = w;
                }
            }
            return Util.vec2(maxWidth + 25, this.connections.length * 25);
        };
        this.connectionsLayout.onTransform = (layout, context) => {
            var width = layout.actualWidth;
            width += this.nameLayout.actualWidth > this.descriptionLayout.actualWidth ? this.nameLayout.actualWidth : this.descriptionLayout.actualWidth;
            if(width < this.MINWIDTH) {
                width = this.MINWIDTH;
            }
            context.translate(width - layout.actualWidth + layout.padding.left, layout.padding.top);
        };
        this.connectionsLayout.onRender = (layout, context) => {
            context.font = "bold 12px Nunito Sans";
            context.textBaseline = "middle";
            for(var i=0; i<this.connections.length; ++i) {
                var connection = this.connections[i];
                context.fillStyle = "#000";
                context.fillText(connection.name, 0.0, i * 25.0 + 12.5);
                context.fillStyle = "#fff";
                context.strokeStyle = this.visualState == RenderableConstants.STATE_NORMAL ? "#bbb" : "#000";
                if(connection.state == RenderableConstants.STATE_HOVERED) {
                    context.fillStyle = "#fcba03";
                    context.strokeStyle = "#fcba03";
                }
                context.lineWidth = 2.0;
                context.beginPath();
                if(context.roundRect) {
                    context.roundRect(layout.width - 20, i * 25 + 2.5, 20, 20, 4);
                } else {
                    context.rect(layout.width - 20, i * 25 + 2.5, 20, 20);
                }
                context.fill();
                context.stroke();
            }
        };
        this.descriptionLayout = new Layout();
        this.descriptionLayout.padding.left = 20;
        this.descriptionLayout.padding.right = 20;
        this.descriptionLayout.padding.bottom = 20;
        this.descriptionLayout.onCalculate = (layout) => {
            if(this.description && this.description.length > 0) {
                this.setFont(14);
                // find max row width
                var rows = this.description.split('\n');
                var maxRowWidth = 0;
                for(var i=0; i<rows.length; ++i) {
                    var rowWidth = this.context.measureText(rows[i]).width;
                    if(rowWidth > maxRowWidth) {
                        maxRowWidth = rowWidth;
                    }
                }
                if(maxRowWidth > this.MAXWIDTH) {
                    maxRowWidth = this.MAXWIDTH;
                }
                var whiteSpaceWidth = this.context.measureText(" ").width;              
                var currentWidth = this.nameLayout.actualWidth + this.connectionsLayout.actualWidth;
                var maxWidth = maxRowWidth;
                if(currentWidth > maxWidth) {
                    maxWidth = currentWidth - (layout.padding.left + layout.padding.right);
                }
                if(this.connections.length > 0) {
                    var w = this.width - (layout.padding.left + layout.padding.right + this.connectionsLayout.width + this.connectionsLayout.padding.right * 0.5);
                    if(maxWidth > w) {
                        maxWidth -= this.connectionsLayout.width + this.connectionsLayout.padding.right * 0.5;
                    }
                }
                // process each row to extract sub rows
                var process = (row) => {
                    var raw = row.split(' ');
                    var words = [];
                    for(var i=0; i<raw.length; ++i) {
                        var r = raw[i];
                        words.push({
                            text: r,
                            width: this.context.measureText(r).width
                        });
                    }
                    var rows = [words[0].text];
                    var currentLength = words[0].width;
                    for(var i=1; i<words.length; ++i) {
                        if((currentLength + whiteSpaceWidth + words[i].width) > (maxWidth + 1)) {
                            // new row
                            currentLength = words[i].width;
                            rows.push(words[i].text);
                        } else {
                            // append on previous row
                            currentLength += whiteSpaceWidth + words[i].width;
                            rows[rows.length - 1] += " " + words[i].text;
                        }
                    }
                    return rows;
                };                
                var total = [];
                for(var i=0; i<rows.length; ++i) {
                    total.push(...process(rows[i]));
                }
                // save total rows for rendering
                layout.rows = total;
                var offset = this.name.length == 0 ? 35.0 : 0.0;
                return Util.vec2(maxWidth, layout.rows.length * 16 + offset);
            }
            layout.rows = [];
            return Util.vec2(0.0, 0.0);
        };
        this.descriptionLayout.onTransform = (layout, context) => {
            var offset = this.name.length == 0 ? 35.0 : 0.0;
            context.translate(layout.padding.left, this.nameLayout.actualHeight + layout.padding.top + offset);
        };
        this.descriptionLayout.onRender = (layout, context) => {
            if(layout.rows) {
                context.fillStyle = "#000";
                context.font = "14px Nunito Sans";
                context.textBaseline = "normal";
                var y = 0.0;
                for(var i=0; i<layout.rows.length; ++i) {
                    context.fillText(layout.rows[i], 0.0, y);
                    y += 16.0;
                }
            }
        };
        this.tagsLayout = new Layout();
        this.tagsLayout.padding.left = 20;
        this.tagsLayout.padding.right = 20;
        this.tagsLayout.padding.bottom = 5;
        this.tagsLayout.onCalculate = (layout) => {
            this.setFont(14);
            var whiteSpaceWidth = this.context.measureText(" ").width;
            var currentLength = 0;
            var height = this.tags.length > 0 ? 16 : 0;
            var maxWidth = this.width - (layout.padding.left + layout.padding.right);
            var x = 0;
            var y = 0;
            var calculatedTags = [];
            for(var i=0; i<this.tags.length; ++i) {
                var w = this.context.measureText("#" + this.tags[i]).width;
                if((currentLength + w + whiteSpaceWidth) >= maxWidth) {
                    // new row
                    currentLength = w + whiteSpaceWidth;
                    x = 0;
                    y += 16;
                    height += 16;
                } else {
                    currentLength += w + whiteSpaceWidth;
                }
                calculatedTags.push({
                    text: "#" + this.tags[i],
                    x: x,
                    y: y
                });
                x = currentLength;
            }
            layout.calculatedTags = calculatedTags;
            return Util.vec2(maxWidth, height);
        };
        this.tagsLayout.onTransform = (layout, context) => {
            context.translate(layout.padding.left, this.height - layout.actualHeight);
        };
        this.tagsLayout.onRender = (layout, context) => {
            context.fillStyle = "#030DFE";
            context.font = "bold 14px Nunito Sans";
            context.textBaseline = "normal";
            for(var i=0; i<layout.calculatedTags.length; ++i) {
                var tag = layout.calculatedTags[i];
                context.fillText(tag.text, tag.x, tag.y);
            }
        };

    }

    numIncomingConnections() {
        return this.inboundConnections.length;
    }

    numOutgoingConnections() {
        var count = 0;
        for(var i=0; i<this.connections.length; ++i) {
            count += this.connections[i].connections.length;
        }
        return count;
    }

    getPath() {
        // return an array with all cards connected from root to this card
        var path = [];
        this.inboundConnections.forEach((connetion) => {
            var p = connetion.card.getPath();
            path.push(...p);
        });
        path.push(this);
        return path;
    }

    getBranchPaths() {
        return this.connections.flatMap((node) => {
            return node.connections.flatMap((connection) => {
                return {
                    node: connection,
                    children: connection.getBranchPaths()
                };
            });
        });
    }

    isExpandable() {
        // TODO: take tags into account?
        return this.name.length > 0 && this.description && this.description.length > 0;
    }

    getConnection(name) {
        return this.connections.find((x) => {
            return x.name == name;
        });
    }

    addConnectionNode(name) {
        if(!this.getConnection(name)) {
            var node = {
                card: this,
                name: name,
                state: RenderableConstants.STATE_NORMAL,
                connections: [],
                connect: null,
                disconnect: null
            };
            var connect = (card) => {
                if(node.connections.indexOf(card) == -1) {
                    node.connections.push(card);
                    card.inboundConnections.push(node);
                }
            };
            var disconnect = (card) => {
                var index = node.connections.indexOf(card);
                if(index >= 0) {
                    // remove the connection to the card
                    node.connections.splice(index, 1);
                    // remove inbound connections to the card
                    index = card.inboundConnections.indexOf(node);
                    if(index >= 0) {
                        card.inboundConnections.splice(index, 1);
                    }
                }
            };
            node.connect = connect;
            node.disconnect = disconnect;
            this.connections.push(node);
            if(this.context) {
                this.connectionsLayout.calculate();
                this.onSizeChanged();
                this.tagsLayout.calculate();
                this.onSizeChanged();
            }
            return node;
        }
    }

    removeConnectionNode(name) {
        var node = this.getConnection(name);
        if(node) {
            var index = this.connections.indexOf(node);
            this.connections.splice(index, 1);
            this.connectionsLayout.calculate();
            this.onSizeChanged();
            this.tagsLayout.calculate();
            this.onSizeChanged();
        }
    }

    toggleTag(tag) {
        var index = this.tags.indexOf(tag);
        if(index >= 0) {
            this.tags.splice(index, 1);
        } else {
            this.tags.push(tag);
        }
        this.tagsLayout.calculate();
        this.onSizeChanged();
    }

    hasTag(tag) {
        return this.tags.indexOf(tag) >= 0;
    }

    setName(name) {
        this.name = name.trim();
        this.nameLayout.calculate();
        this.onSizeChanged();
    }

    setDescription(description) {
        this.description = description.trim();
        this.descriptionLayout.calculate();
        if(this.description.length == 0) {
            this.expanded = false;
        }
        this.onSizeChanged();
        this.tagsLayout.calculate();
        this.onSizeChanged();
    }

    setExpanded(expanded) {
        this.expanded = expanded;
        this.onSizeChanged();
    }

    setFont(size) {
        this.context.font = "" + size + "px Nunito Sans";
    }

    getNodeOutputLocation(node) {
        var index = this.connections.indexOf(node);
        if(index != -1) {
            var x = this.x + this.width - this.connectionsLayout.padding.right;
            var y = this.y + this.connectionsLayout.padding.top + index * 25.0 + 12.5;
            return Util.vec2(x, y);
        }
        return null;
    }

    hitTestConnectionNode(point) {
        var x = this.x + this.width - this.connectionsLayout.padding.right - 20;
        var y = this.y + this.connectionsLayout.padding.top + 2.5;
        for(var i=0; i<this.connections.length; ++i) {
            if(point.x > x && point.x < x + 20 && point.y > y && point.y < y + 20) {
                return this.connections[i];
            }
            y += 25.0;
        }
        return null;
    }

    hitTest(point) {
        if(this.hitTestBounds(point)) {
            return true;
        }
        return this.hitTestConnectionNode(point) != null;
    }

    onClick(mouse) {
        if(this.isExpandable()) {
            // expand button
            var hw = this.width * 0.5;
            if(mouse.y > this.height - 24.0 && mouse.x > hw - 20.0 && mouse.x < hw + 20.0) {
                this.setExpanded(!this.expanded);
            }
        }
        // edit button
        if(mouse.x > this.width - 36.0 && mouse.x < this.width - 8.0 && mouse.y < 36.0 && mouse.y > 8.0) {
            EventDispatcher.fire(Constants.EVENT_EDIT_CARD, this);
        }
    }

    onVisualStateChanged() {
        if(this.visualState == RenderableConstants.STATE_NORMAL) {
            if(this.hoveredConnection) {
                this.hoveredConnection.state = RenderableConstants.STATE_NORMAL;
                this.hoveredConnection = null;
            }
        }
    }
    
    onMouseMove(mouse) {
        if(this.isExpandable()) {
            // expand button
            var hw = this.width * 0.5;
            if(mouse.y > this.height - 24.0 && mouse.x > hw - 20 && mouse.x < hw + 20) {
                this.canvas.style.cursor = "pointer";
            }
        }
        // edit button
        if(mouse.x > this.width - 36.0 && mouse.x < this.width - 8.0 && mouse.y < 36.0 && mouse.y > 8.0) {
            this.canvas.style.cursor = "pointer";
        }
        // connection nodes, move point to world for hit test
        var node = this.hitTestConnectionNode(Util.vec2Add(Util.vec2(this.x, this.y), mouse));
        if(this.hoveredConnection && this.hoveredConnection != node) {
            this.hoveredConnection.state = RenderableConstants.STATE_NORMAL;
        }
        this.hoveredConnection = node;
        if(this.hoveredConnection) {
            this.canvas.style.cursor = "pointer";
            this.hoveredConnection.state = RenderableConstants.STATE_HOVERED;
        }
    }

    onSizeChanged() {
        this.width = this.nameLayout.actualWidth > this.descriptionLayout.actualWidth ? this.nameLayout.actualWidth : this.descriptionLayout.actualWidth;
        this.width += this.connectionsLayout.actualWidth;
        if(this.width < this.MINWIDTH) {
            this.width = this.MINWIDTH;
        }
        this.height = this.nameLayout.actualHeight;
        if(this.expanded || this.name.length == 0) {
            this.height = this.nameLayout.actualHeight + this.descriptionLayout.actualHeight;
        }
        if(this.height < this.connectionsLayout.actualHeight) {
            this.height = this.connectionsLayout.actualHeight;
        }
        if(this.tags.length > 0) {
            this.height += this.tagsLayout.actualHeight;
        }
    }

    onAttached() {
        // force a size calculation when the card is added to world
        this.connectionsLayout.calculate();
        this.nameLayout.calculate();
        this.descriptionLayout.calculate();
        this.tagsLayout.calculate();
        this.onSizeChanged();
    }

    onDelete() {
        for(var i=0; i<this.inboundConnections.length; ++i) {
            var index = this.inboundConnections[i].connections.indexOf(this);
            if(index >= 0) {
                this.inboundConnections[i].connections.splice(index, 1);
            }
        }
    }

    onRender(context) {
        context.fillStyle = "#fff";
        context.strokeStyle = this.visualState == RenderableConstants.STATE_NORMAL ? this.borderColor : "#000";
        context.lineWidth = this.borderThickness;
        // if the card is dragged, draw shadow for body
        if(this.visualState == RenderableConstants.STATE_DRAGGED) {
            context.shadowColor = "#999";
            context.shadowBlur = 15;
        }
        // background
        context.beginPath();
        if(context.roundRect) {
            context.roundRect(0, 0, this.width, this.height, 8);
        } else {
            // NOTE: firefox does not support roundRect
            context.rect(0, 0, this.width, this.height);
        }
        context.fill();
        context.shadowColor = null;
        context.shadowBlur = null;
        context.stroke();
        // content
        if(this.name.length > 0) {
            this.nameLayout.render(context);
        }
        this.connectionsLayout.render(context);
        if(this.expanded || this.name.length == 0) {
            this.descriptionLayout.render(context);
        }
        this.tagsLayout.render(context);
        if(this.visualState == RenderableConstants.STATE_HOVERED || this.visualState == RenderableConstants.STATE_DRAGGED) {
            context.drawImage(Constants.IMG_EDIT, this.width - 34.0, 10.0);
            if(this.isExpandable()) {
                context.drawImage(this.expanded ? Constants.IMG_ANGLE_UP : Constants.IMG_ANGLE_DOWN, this.width * 0.5 - 12.0, this.height - 24.0);
            }
        }
    }

}

export default  Card;