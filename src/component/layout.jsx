
class Layout {

    constructor() {
        this.width = 0;
        this.height = 0;
        this.actualWidth = 0;
        this.actualHeight = 0;
        this.padding = {
            left: 0,
            right: 0,
            top: 0,
            bottom: 0
        };
        this.onCalculate = null;
        this.onRender = null;
        this.onTransform = null;
    }

    calculate() {
        if(this.onCalculate) {
            var size = this.onCalculate(this);
            this.width = size.x;
            this.height = size.y;
        }
        this.actualWidth = this.width + this.padding.left + this.padding.right;
        this.actualHeight = this.height + this.padding.top + this.padding.bottom;
    }

    render(context) {
        context.save();
        if(this.onTransform) {
            this.onTransform(this, context);
        }
        if(this.onRender) {
            this.onRender(this, context);
        }
        context.restore();
    }

}

export default Layout;