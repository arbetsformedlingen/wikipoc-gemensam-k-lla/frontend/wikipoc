import React from 'react';
import ReactDOM from 'react-dom';
import Support from './support.jsx';
import App from './context/app.jsx';
import Constants from './context/constants.jsx';
import EventDispatcher from './context/event_dispatcher.jsx';
import Menu from './content/menu.jsx';
import Start from './content/start.jsx';
import Login from './content/login.jsx';
import Register from './content/register.jsx';
import RegisterStatus from './content/register_status.jsx';
import Project from './content/project/project.jsx';
import Material from './content/material/material.jsx';
import Explore from './content/explore/explore.jsx';

class Index extends React.Component { 

	constructor() {
        super();
        Support.init();
        this.state = {
            page: Constants.PAGE_START,
        };
        App.onInit();
    }

    componentDidMount() {
        EventDispatcher.add(this.onPageChanged.bind(this), Constants.EVENT_SET_PAGE);
    }

    onPageChanged(page) {
        App.page = page;
        this.setState({page: page});
    }

    onLoginClicked() {
        this.setState({page: Constants.PAGE_LOGIN});
    }

    renderHeader() {
        return (
            <div className="header">
                <div>WikiPoC</div>
                {App.user &&
                    <div className="header_user">
                        <div>{Constants.ICON_USER}</div>
                        <div>{App.user.name}</div>
                    </div>
                }
                {(!App.user && this.state.page != Constants.PAGE_LOGIN) &&
                    <div 
                        className="header_login"
                        onPointerUp={this.onLoginClicked.bind(this)}>
                        Logga in
                    </div>
                }
            </div>
        );
    }

    renderContentBody() {
        switch(this.state.page) {
            case Constants.PAGE_PROJECT:
            case Constants.PAGE_PROJECT_EXPLORE:
            case Constants.PAGE_PROJECT_VIEW:
            case Constants.PAGE_PROJECT_EDIT:
                return <Project/>;
            case Constants.PAGE_EXPLORE:
                return <Explore/>;
            case Constants.PAGE_MATERIAL:
            case Constants.PAGE_MATERIAL_EXPLORE:
            case Constants.PAGE_MATERIAL_VIEW:
                return <Material/>;
        }
    }

    renderBody() {
        switch(this.state.page) {
            case Constants.PAGE_START:
                return <Start/>;
            case Constants.PAGE_LOGIN:
                return <Login/>;
            case Constants.PAGE_REGISTER:
                return <Register/>;
            case Constants.PAGE_REGISTER_STATUS:
                return <RegisterStatus/>;
        }
        return (
            <div className="content">
                <Menu/>
                {this.renderContentBody()}
            </div>
        );
    }

    render() {        
        return (
            <div className="main">
                {this.renderHeader()}
                {this.renderBody()}
            </div>
        );
    }
	
}

ReactDOM.render(<Index/>, document.getElementById('content'));