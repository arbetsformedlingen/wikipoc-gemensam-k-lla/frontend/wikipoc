FROM docker.io/library/node:23.9.0-alpine3.21

COPY . /app
WORKDIR /app

# this part just makes sure there arent any compile errors
RUN apk update &&\
    npm install &&\
    npm run release

# here we more or less ignore the build, and run a node instance, for the time being
EXPOSE 8000
COPY config.js  /mnt/volumemount/config.js
RUN ln -sf /mnt/volumemount/config.js config.js


CMD npm run prod
