var fs = require('fs');

var errorLog = function(v) {
	if(v) {
		console.log(v);
	}
};

var replaceHtmlContent = function(htmlFile, files) {
	fs.readFile(htmlFile, 'utf8', function (err, data) {
		if (err) {
			return console.log(err);
		}
		var result = data;		
		for(var i=0; i<files.length; ++i) {
			if(files[i].replace) {
				result = result.replace(files[i].src, files[i].dst);
			}
		}
		fs.writeFile(htmlFile, result, 'utf8', errorLog);
	});
};

var readVersion = function(callback) {
	fs.readFile("version", 'utf8', function(err, data) {
		if (err) {
			return console.log(err);
		}
		callback(data);
	});
};

try {
	readVersion(function(data) {
		var version = data;
		replaceHtmlContent('index.html', [{src: '<title>Taxonomy</title>', dst: '<title>Taxonomy ' + version + '</title>', replace: true}]);
	});
} catch(e) {
	console.log(e);
}